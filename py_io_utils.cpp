#include </usr/local/opt/python@3.11/Frameworks/Python.framework/Versions/3.11/include/python3.11/Python.h>
#include <vector>

PyObject* to_list(const std::vector<float>& vector_1d)
{
  PyObject* result = PyList_New(0);
  for (size_t i = 0; i < vector_1d.size(); ++i)
  {
    PyList_Append(result, PyFloat_FromDouble(vector_1d[i]));
  }
  return result;
}

PyObject* to_list(const std::vector<std::vector<float>>& vector_2d)
{
  PyObject* result = PyList_New(0);
  for (size_t i = 0; i < vector_2d.size(); ++i)
  {
    PyObject* py_list_element = PyList_New(0); // is list itself
    for (size_t j = 0; j < vector_2d[i].size(); ++j)
    {
      PyList_Append(py_list_element, PyFloat_FromDouble(vector_2d[i][j]));
    }
    PyList_Append(result, py_list_element);
  }
  return result;
}
