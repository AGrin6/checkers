#include </usr/local/opt/python@3.11/Frameworks/Python.framework/Versions/3.11/include/python3.11/Python.h>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <tuple>
#include <array>
#include <stack>
#include <unordered_set>
#include <iterator>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <chrono>                                              /* time_track */
#include "node.h"
#include "game.h"
#include "hashing.h"
#include "player.h"
#include "py_io_utils.h"

template <typename T>
std::string to_argument(const std::vector<std::vector<T>>&);

void set_zero(char data[7], uint8_t bit)
{
  data[bit / 8] &= ~(1 << (bit % 8));
}

void set_zero(std::array<char, 7>& data, uint8_t bit)
{
  data[bit / 8] &= ~(1 << (bit % 8));
}

void set_unit(char data[7], uint8_t bit)
{
  data[bit / 8] |= 1 << (bit % 8);
}

void set_unit(std::array<char, 7>& data, uint8_t bit)
{
  data[bit / 8] |= 1 << (bit % 8);
}

bool get_bit(const char data[7], uint8_t bit)
{
  return (data[bit / 8] >> (bit % 8)) & 1;
}

BoardMini::BoardMini(std::initializer_list<std::initializer_list<uint8_t>> init_board)
{
  assert(true);                                                                /* debug_assert */
  if (init_board.begin()->size() == 0) {return;}
  if (*init_board.begin()->begin() == 1)
  {
    data[0] |= 1 << 0;
    data[0] &= ~(1 << 1);
    data[0] |= 1 << 2;
  }
  else if (*init_board.begin()->begin() == 2)
  {
    data[0] |= 1 << 0;
    data[0] &= ~(1 << 1);
    data[0] &= ~(1 << 2);
  }
  else if (*init_board.begin()->begin() == 3)
  {
    data[0] |= 1 << 0;
    data[0] |= 1 << 1;
    data[0] |= 1 << 2;
  }
  else if (*init_board.begin()->begin() == 4)
  {
    data[0] |= 1 << 0;
    data[0] |= 1 << 1;
    data[0] &= ~(1 << 2);
  }
  else
  {
    assert(*init_board.begin()->begin() == 0);
    data[0] &= ~(1 << 0);
    data[0] &= ~(1 << 1);
    data[0] &= ~(1 << 2);
  }

  assert(*(init_board.begin()->begin() + 1) == 0);

  for (size_t j = 2; j < 6; ++j)
  {
    if (j % 2 == 0)
    {
      if (*(init_board.begin()->begin() + 2) == 1)
      {
        set_unit(data, 3 * (j / 2));
        set_zero(data, 3 * (j / 2) + 1);
        set_unit(data, 3 * (j / 2) + 2);
      }
      else if (*(init_board.begin()->begin() + 2) == 2)
      {
        set_unit(data, 3 * (j / 2));
        set_zero(data, 3 * (j / 2) + 1);
        set_zero(data, 3 * (j / 2) + 2);
      }
      else if (*(init_board.begin()->begin() + 2) == 3)
      {
        set_unit(data, 3 * (j / 2));
        set_unit(data, 3 * (j / 2) + 1);
        set_unit(data, 3 * (j / 2) + 2);
      }
      else if (*(init_board.begin()->begin() + 2) == 4)
      {
        set_unit(data, 3 * (j / 2));
        set_unit(data, 3 * (j / 2) + 1);
        set_zero(data, 3 * (j / 2) + 2);
      }
      else
      {
        assert(*(init_board.begin()->begin() + 2) == 0);
        set_zero(data, 3 * (j / 2));
        set_zero(data, 3 * (j / 2) + 1);
        set_zero(data, 3 * (j / 2) + 2);
      }
    }
    else
    {
      assert(*(init_board.begin()->begin() + j) == 0);
    }
  }

  for (size_t i = 1; i < 6; ++i)
  {
    for (size_t j = 0; j < 6; ++j)
    {
      if ((i + j) % 2 == 0)
      {
        if (*((init_board.begin() + i)->begin() + j) == 1)
        {
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));       // i + j blah blah
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (*((init_board.begin() + i)->begin() + j) == 2)
        {
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (*((init_board.begin() + i)->begin() + j) == 3)
        {
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (*((init_board.begin() + i)->begin() + j) == 4)
        {
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
          set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else
        {
          assert(*((init_board.begin() + i)->begin() + j) == 0);
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2));
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
      }
      else
      {
        assert(*((init_board.begin() + i)->begin() + j) == 0);
      }
    }
  }
  // two remaining bits to make equal boards have equal encodings
  set_zero(data, 54);
  set_zero(data, 55);
}

inline uint8_t get_value(const char data[7], uint8_t i, uint8_t j)
{
  if (((i + j) % 2 == 1) || !get_bit(data, 3 * (3 * i + (j - (i % 2)) / 2)))
  {
    return 0;
  }
  else
  {
    if (get_bit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1))
    {
      if (get_bit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2))
      {
        return 3;
      }
      else
      {
        return 4;
      }
    }
    else
    {
      if (get_bit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2))
      {
        return 1;
      }
      else
      {
        return 2;
      }
    }
  }
}

std::array<std::array<uint8_t, 6>, 6> to_board(const std::array<char, 7>& data)
{
  std::array<std::array<uint8_t, 6>, 6> result;
  for (size_t i = 0; i < 6; ++i)
  {
    for (size_t j = 0; j < 6; ++j)
    {
      result[i][j] = get_value(data.data(), i, j);
    }
  }
  return result;
}

uint8_t BoardMini::get_value(uint8_t i, uint8_t j) const
{
  return ::get_value(data, i, j);
}

void BoardMini::set_value(uint8_t i, uint8_t j, uint8_t value)
{
  assert((i + j) % 2 == 0);                                                    /* debug_assert */
  if (value == 0)
  {
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2));
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
  }
  else if (value == 1)
  {
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
  }
  else if (value == 2)
  {
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
  }
  else if (value == 3)
  {
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
  }
  else if (value == 4)
  {
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2));
    set_unit(data, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
    set_zero(data, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
  }
  else assert(false);                                                          /* debug_assert */
}

bool BoardMini::operator==(const BoardMini& other) const
{
  return std::equal(std::begin(data), std::end(data), std::begin(other.data));
}

std::array<char, 7> BoardMini::compress() const
{
  std::array<char, 7> result;
  std::copy(std::begin(data), std::end(data), std::begin(result));
  return result;
}

std::ostream &operator<<(std::ostream& os, const std::array<char, 7>& item)
{
  return os << "[" << unsigned(item[0]) << " " << unsigned(item[1]) << " " << unsigned(item[2])
            << " " << unsigned(item[3]) << " " << unsigned(item[4]) << " " << unsigned(item[5])
            << " " << unsigned(item[6]);
}

template <typename T>
std::string to_string(const std::vector<std::pair<T, T>>& array)
{
  std::string result = "(";
  for (auto value : array)
  {
    result += "(";
    result += std::to_string(value.first);
    result += ", ";
    result += std::to_string(value.second);
    result += ")";
  }
  result += ")";
  return result;
}

template <typename T>
std::string to_string(const std::vector<std::vector<std::pair<T, T>>>& array)
{
  std::string result = "(";
  for (auto option : array)
  {
    result += "(";
    for (auto value : option)
    {
      result += "(";
      result += std::to_string(value.first);
      result += ", ";
      result += std::to_string(value.second);
      result += ")";
    }
    result += ")";
  }
  result += ")";
  return result;
}

template <typename BoardType>
std::string to_string(const CheckersPosition<BoardType>& position)
{
  std::string result = ((position.current_player == 1) ? "      white  \n" : "      black  \n");
  result += "   * * * * * *\n";
  for (size_t i = 0; i < size<0>(position.field); ++i)
  {
    result += " * ";
    for (size_t j = 0; j < size<1>(position.field); ++j)
    {
      result += std::to_string(get_value(position.field, size<0>(position.field) - i - 1, j));
      result += " ";
    }
    result += "*\n";
  }
  result += "   * * * * * *\n";
  return result;
}

template <typename T>
std::string to_string(const CheckersSituation<T>& situation)
{
  std::string result = ((situation.position.current_player == 1) ?
                         "      white  \n" : "      black  \n");
  result += "   * * * * * *\n";
  for (size_t i = 0; i < situation.position.field.size(); ++i)
  {
    result += " * ";
    for (size_t j = 0;
         j < situation.position.field[situation.position.field.size() - i - 1].size(); ++j)
    {
      result += std::to_string(
                             situation.position.field[situation.position.field.size() - i - 1][j]);
      result += " ";
    }
    result += "*\n";
  }
  result += "   * * * * * *\n";
  return result;
}

template <typename T>
std::string to_string(const T& situation, bool is_full)
{
  std::string result = to_string(situation);

  if (is_full)
  {
    for (auto it = situation.previous.cbegin(); it != situation.previous.cend(); ++it)
    {
      result += to_string(*it);
    }
  }
  return result;
}

// taken from SO
std::size_t std::hash<CheckersPosition<std::vector<std::vector<square_t>>>>::operator()(
                           const CheckersPosition<std::vector<std::vector<square_t>>>& input) const
{
  size_t seed = input.current_player;
  seed ^= std::hash<size_t>()(input.field.size()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  seed ^= std::hash<size_t>()(input.field[0].size()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  for (size_t i = 0; i < input.field.size(); ++i)
  {
    for (size_t j = 0; j < input.field[0].size(); ++j)
    {
      seed ^= std::hash<size_t>()(input.field[i][j]) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
  }
  return seed;
}

std::size_t std::hash<CheckersPosition<std::vector<std::vector<size_t>>>>::operator()(
                             const CheckersPosition<std::vector<std::vector<size_t>>>& input) const
{
  size_t seed = input.current_player;
  seed ^= std::hash<size_t>()(input.field.size()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  seed ^= std::hash<size_t>()(input.field[0].size()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  for (size_t i = 0; i < input.field.size(); ++i)
  {
    for (size_t j = 0; j < input.field[0].size(); ++j)
    {
      seed ^= std::hash<size_t>()(input.field[i][j]) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }
  }
  return seed;
}

std::size_t std::hash<CheckersPosition<BoardMini>>::operator()(
                                                    const CheckersPosition<BoardMini>& input) const
{
  size_t seed = input.current_player;
  seed ^= std::hash<size_t>()(size<0>(input.field)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  seed ^= std::hash<size_t>()(size<1>(input.field)) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  for (size_t i = 0; i < size<0>(input.field); ++i)
  {
    for (size_t j = 0; j < size<1>(input.field); ++j)
    {
      seed ^= std::hash<size_t>()(get_value(input.field, i, j)) + 0x9e3779b9 + (seed << 6)
              + (seed >> 2);
    }
  }
  return seed;
}

template <typename T>
CheckersPositionStable<T>::CheckersPositionStable(
                           const T& field, uint8_t player): field(field), current_player(player) {}

template <typename T>
bool CheckersPositionStable<T>::operator==(const CheckersPosition<T>& other) const
{
  return ((field == other.field) && (current_player == other.current_player));
}

template <typename T>
std::array<char, 7> CheckersPositionStable<T>::compress() const
{
  std::array<char, 7> result = ::compress(field);
  if (current_player == 1)
  {
    set_zero(result, 54);
  }
  else // current_player == 2
  {
    set_unit(result, 54);
  }
  return result;
}

template class CheckersPositionStable<std::vector<std::vector<size_t>>>;
template class CheckersPositionStable<std::vector<std::vector<uint8_t>>>;
template class CheckersPositionStable<BoardMini>;

template <typename T>
std::vector<CheckersMove> get_options(const CheckersPosition<T>& position)
{
  InitialNode<T> initial_root(position.current_player == 1);
  std::vector<CheckersMove> result = depth_width_search<T>(&initial_root, position.field);
  return result;
}

template std::vector<CheckersMove> get_options<std::vector<std::vector<uint8_t>>>(
                              const CheckersPosition<std::vector<std::vector<uint8_t>>>& position);

template std::vector<CheckersMove> get_options<std::vector<std::vector<size_t>>>(
                               const CheckersPosition<std::vector<std::vector<size_t>>>& position);

template <typename T>
void print_board(const CheckersPositionStable<T>& position)
{
  print_board(position.field);
}

template <typename T>
void show_board(const CheckersPositionStable<T>& position)
{
  show_board(position.field);
}

template <typename T>
std::string to_argument(const CheckersPositionStable<T>& position)
{
  return to_argument(position.field);
}

// forgot & in previous variable in previous version (!!!)
template <typename BoardType>
status_t check_victory(const CheckersPosition<BoardType>& position,
                       const std::unordered_set<CheckersPosition<BoardType>>& previous)
{
  if (get_options(position).empty())
  {
    if (position.current_player == 1) return BLACK;
    else /*==2*/ return WHITE;
  }
  else if (previous.contains(position))
  {
    return DRAW;
  }
  return UNKNOWN;
}

template <typename BoardType>
status_t check_victory(const CheckersSituation<BoardType>& situation)
{
  // std::cout << "DEBUG: check_victory(...)\n";
  // std::cout << "DEBUG: situation.previous.size(): " << situation.previous.size() << "\n";
  if (get_options(situation.position).empty())
  {
    if (situation.position.current_player == 1) return BLACK;
    else /* ayer == 2 */ return WHITE;
  }
  else if (situation.previous.contains(situation.position))
  {
    std::cout << "DEBUG: return DRAW;\n";
    return DRAW;
  }
  return UNKNOWN;
}

template <typename T>
status_t check_victory(const CheckersGame<T>& game)
{
  return game.check_victory();
}

template status_t check_victory<std::vector<std::vector<square_t>>>(
                                       const CheckersGame<std::vector<std::vector<square_t>>>&);

// status_t check_victory(const CheckersSituationVer1& situation)
// {
//   if (get_options(situation.position).empty())
//   {
//     if (situation.position.current_player == 1) return BLACK;
//     else /* rent_player == 2 */ return WHITE;
//   }
//   else if (std::find(situation.previous.cbegin(), situation.previous.cend(),
//                      situation.position) != situation.previous.cend())
//   {
//     return DRAW;
//   }
//   return UNKNOWN;
// }

template <typename T>
CheckersSituation<T>::CheckersSituation(): position(CheckersGame<T>::get_initial_position()) {}

template <typename T>
CheckersSituation<T>::CheckersSituation(const CheckersPosition<T>& position): position(position) {}

template class CheckersSituation<std::vector<std::vector<size_t>>>;
template class CheckersSituation<std::vector<std::vector<uint8_t>>>;

template <typename T>
void print_board(const CheckersSituation<T>& situation)
{
  print_board(situation.position);
}

template <typename T>
void show_board(const CheckersSituation<T>& situation)
{
  show_board(situation.position);
}

template <typename T>
std::string to_argument(const CheckersSituation<T>& situation)
{
  return to_argument(situation.position);
}

template <typename T>
std::vector<CheckersMove> get_options(const CheckersSituation<T>& situation)
{
  return get_options(situation.position);
}

template <typename T>
std::vector<CheckersMove> CheckersGame<T>::get_options() const
{
  return ::get_options(situation_);
}

// template <typename BoardType>
// BoardType CheckersGame<BoardType>::get_initial_board()
// {
//   return BoardType({{1, 0, 1, 0, 1, 0},
//                     {0, 1, 0, 1, 0, 1},
//                     {0, 0, 0, 0, 0, 0},
//                     {0, 0, 0, 0, 0, 0},
//                     {2, 0, 2, 0, 2, 0},
//                     {0, 2, 0, 2, 0, 2}}, 1);
// }

template <typename T>
CheckersPosition<T> CheckersGame<T>::get_initial_position()
{
  return CheckersPosition<T>({{1, 0, 1, 0, 1, 0},
                              {0, 1, 0, 1, 0, 1},
                              {0, 0, 0, 0, 0, 0},
                              {0, 0, 0, 0, 0, 0},
                              {2, 0, 2, 0, 2, 0},
                              {0, 2, 0, 2, 0, 2}}, 1);
}

template <typename T>
CheckersGame<T>::CheckersGame(): situation_(get_initial_position()) {};

template <typename T>
std::string to_argument(const std::vector<std::vector<T>>& field)
{
  std::string result;
  for (size_t i = 0; i < field.size(); ++i)
  {
    for (size_t j = 0; j < field[0].size(); ++j)
    {
      result += std::to_string(field[i][j]);
      result += " ";
    }
  }
  result.pop_back();
  return result;
}

template <typename BoardType>
std::vector<float> to_vector(const BoardType& field)
{
  std::vector<float> result;
  for (size_t i = 0; i < size<0>(field); ++i)
  {
    for (size_t j = 0; j < size<1>(field); ++j)
    {
      result.push_back(get_value(field, i, j));
    }
  }
  return result;
}

template <typename T>
std::vector<float> to_vector(const CheckersPositionStable<T>& position)
{
  return to_vector(position.field);
}

template <typename T>
std::vector<float> to_vector(const CheckersSituation<T>& situation)
{
  return to_vector(situation.position);
}

template <typename T>
void CheckersGame<T>::draw_board() const
{
  print_board(situation_);
  // show_board(situation_); // temporarily removed due graphics option and lack of terminal space
  // using another graphics output method
  // system(("/usr/local/bin/python3 show_board.py " + to_argument(situation_)).c_str());
}

template <typename T>
CheckersMove get_graphical_input(const CheckersSituation<T>& situation)
{
  std::string move_code;

  char module_name[] = "show_board";
  PyObject* py_name = PyUnicode_DecodeFSDefault(module_name);
  PyObject* py_module = PyImport_Import(py_name);
  Py_DECREF(py_name);

  if (py_module != NULL)
  {
    PyObject* py_function = PyObject_GetAttrString(py_module, "show_board_list");
    if (py_function && PyCallable_Check(py_function))
    {
      PyObject* py_argument = to_list(to_vector(situation));
      PyObject* py_result = PyObject_CallOneArg(py_function, py_argument);
      Py_DECREF(py_argument);
      if (py_result != NULL)
      {
        move_code = PyBytes_AsString(PyUnicode_AsASCIIString(py_result));
        Py_DECREF(py_result);
      }
      else
      {
        Py_DECREF(py_function);
        Py_DECREF(py_module);
        PyErr_Print();
        std::cout << "showing board funciton failed\n";
        exit(1);
      }
    }
  }
  return to_checkers_move(move_code);
}

template <typename T>
CheckersMove CheckersGame<T>::graphical_input() const
{
  CheckersMove result = get_graphical_input(situation_); // add do-while move code correct
  return result;
}

std::size_t size_t_diff(std::size_t first, std::size_t second)
{
  return first > second ? first - second : second - first;
}

int size_t_step(std::size_t first, std::size_t second)
{
  return first > second ? 1 : -1;
}

template <typename BoardType>
void apply_move(const CheckersMove& list_of_squares, BoardType& board)
{
  uint8_t piece_code = get_value(board, list_of_squares[0].first, list_of_squares[0].second);
  uint8_t current_player = 2 - piece_code % 2; // 1 if white, 2 if black - for consistency
  for (size_t i = 0; i < list_of_squares.size() - 1; ++i)
  {
    int step_x = size_t_step(list_of_squares[i].first, list_of_squares[i + 1].first);
    int step_y = size_t_step(list_of_squares[i].second, list_of_squares[i + 1].second);
    for (size_t j = 0; j < size_t_diff(list_of_squares[i + 1].first, list_of_squares[i].first);
         ++j)
    {
      set_value(board, list_of_squares[i].first - j * step_x, 
                                list_of_squares[i].second - j * step_y, 0);
    }
    set_value(board, list_of_squares.back().first, list_of_squares.back().second,
              piece_code);
    if ((current_player == 1)
        && (list_of_squares[i + 1].first == size<0>(board) - 1))
    {
      set_value(board, list_of_squares.back().first, list_of_squares.back().second, 3);
      piece_code = 3;
    }
    if ((current_player == 2) && (list_of_squares[i + 1].first == 0))
    {
      set_value(board, list_of_squares.back().first, list_of_squares.back().second, 4);
      piece_code = 4;
    }
  }
}

template void apply_move<std::vector<std::vector<uint8_t>>>(const CheckersMove&,
                                                               std::vector<std::vector<uint8_t>>&);
template void apply_move<std::vector<std::vector<size_t>>>(const CheckersMove&,
                                                                std::vector<std::vector<size_t>>&);
template void apply_move<BoardMini>(const CheckersMove&, BoardMini&);

template <typename T>
void apply_move(const CheckersMove& list_of_squares, CheckersPosition<T>& position)
{
  uint8_t piece_code = get_value(position.field,
                                 list_of_squares[0].first, list_of_squares[0].second);
  for (size_t i = 0; i < list_of_squares.size() - 1; ++i)
  {
    int step_x = size_t_step(list_of_squares[i].first, list_of_squares[i + 1].first);
    int step_y = size_t_step(list_of_squares[i].second, list_of_squares[i + 1].second);
    for (size_t j = 0; j < size_t_diff(list_of_squares[i + 1].first, list_of_squares[i].first);
         ++j)
    {
      set_value(position.field, list_of_squares[i].first - j * step_x, 
                                list_of_squares[i].second - j * step_y, 0);
    }
    set_value(position.field, list_of_squares.back().first, list_of_squares.back().second,
              piece_code);
    // std::cout << "current_palyer_: " << position.current_player << "\n";
    // std::cout << "square: " << list_of_squares.back().first << "\n";
    if ((position.current_player == 1)
        && (list_of_squares[i + 1].first == size<0>(position.field) - 1))
    {
      // std::cout << "crowning\n";
      set_value(position.field, list_of_squares.back().first, list_of_squares.back().second, 3);
      piece_code = 3;
    }
    if ((position.current_player == 2) && (list_of_squares[i + 1].first == 0))
    {
      set_value(position.field, list_of_squares.back().first, list_of_squares.back().second, 4);
      piece_code = 4;
    }
  }
  position.current_player = position.current_player % 2 + 1;
}

// typedef std::vector<std::pair<std::size_t, std::size_t>> CheckersMove;

template <typename BoardType>
void apply_move(const CheckersMove& move, CheckersSituation<BoardType>& situation)
{
  situation.previous.insert(situation.position);
  apply_move(move, situation.position);
}

template <typename BoardType>
CheckersPosition<BoardType> make_move(const CheckersMove& move,
                                      const CheckersPosition<BoardType>& position)
{
  CheckersPosition result(position);
  apply_move(move, result);
  return result;
}

CheckersSituation<std::vector<std::vector<square_t>>> make_move(const CheckersMove& move,
                            const CheckersSituation<std::vector<std::vector<square_t>>>& situation)
{
  CheckersSituation result(situation);
  apply_move(move, result);
  return result;
}

template <typename T>
void CheckersGame<T>::make_move(const CheckersMove& list_of_squares)
{
  apply_move(list_of_squares, situation_);
}

template <typename T>
status_t CheckersGame<T>::check_victory() const
{
  return ::check_victory(situation_);
}

template <typename T>
CheckersPosition<T> CheckersGame<T>::get_current_position() const
{
  return situation_.position;
}

template class CheckersGame<std::vector<std::vector<size_t>>>;
template class CheckersGame<std::vector<std::vector<uint8_t>>>;

GameRecording::GameRecording(size_t ai_player_id):
                              ai_player_id_(ai_player_id), moves_sequence_({}), result_(UNKNOWN) {}

void GameRecording::update_moves(const CheckersMove& new_move)
{
  moves_sequence_.push_back(new_move);
}

void GameRecording::update_result(status_t result)
{
  result_ = result;
}

size_t GameRecording::size() const
{
  return moves_sequence_.size();
}

CheckersMove GameRecording::get_move(size_t i) const
{
  return moves_sequence_[i];
}

status_t GameRecording::get_result() const
{
  return result_;
}

Player::Player() {}

template <typename HashMapType>
AutoPlayer<HashMapType>::AutoPlayer(size_t seed)
{
  srand(seed);
  state_to_result_[CheckersGame<AutoBoardType>::get_initial_position()] = UNKNOWN;
}

// Regression analysis
// 1) Figure out position(s) which are already won (or get one manually?)
// 2) Figure out positions which can get to already won position - new reverse move function
// 3) ???
// At least reverse move function may be not that trivial to write

// for effective implementation of current lookup process a data structure which is a mix
// between stack (effective push/pop) and hash table (effective search) is needed

// TODOs: check inline keyword; check performance impact of moving to separate funciton
template <typename HashMapType>
void lookup_step(HashMapType& state_to_result,
                 CheckersPosition<AutoBoardType>& current_position,
                 std::stack<CheckersPosition<AutoBoardType>>& stack_of_positions,
                 std::unordered_set<CheckersPosition<AutoBoardType>>& previous)
{
  std::cout << "lookup_step(...)\n";
  static std::chrono::duration<double, std::milli> section_0(0);               /* time_track */
  static std::chrono::duration<double, std::milli> section_1(0);               /* time_track */
  static std::chrono::duration<double, std::milli> section_1n(0);              /* time_track */
  static std::chrono::duration<double, std::milli> section_2(0);               /* time_track */
  static std::chrono::duration<double, std::milli> section_3(0);               /* time_track */
  status_t victory = check_victory(current_position, previous);
  if (victory != UNKNOWN)
  {
    auto section_0_start = std::chrono::high_resolution_clock::now();          /* time_track */
    state_to_result[current_position] = victory;
    stack_of_positions.pop();
    current_position = stack_of_positions.top();

    /* size_t is_erased = ** W */ previous.erase(current_position);
    // assert(is_erased == 1); // ** W
    auto section_0_end = std::chrono::high_resolution_clock::now();            /* time_track */
    section_0 += (section_0_end - section_0_start);                            /* time_track */
  }
  else
  {
    std::vector<CheckersMove> move_options = get_options(current_position);
    status_t current_result = (current_position.current_player == 1) ? BLACK : WHITE;
    for (auto option : move_options)
    {
      auto section_1_start = std::chrono::high_resolution_clock::now();        /* time_track */
      CheckersPosition next_position = CheckersPosition(::make_move(option, current_position));
      auto section_1_end = std::chrono::high_resolution_clock::now();          /* time_track */
      section_1 += (section_1_end - section_1_start);                          /* time_track */

      auto section_1n_start = std::chrono::high_resolution_clock::now();       /* time_track */
      bool is_position_seen = state_to_result.contains(next_position);
      auto section_1n_end = std::chrono::high_resolution_clock::now();         /* time_track */
      section_1n += (section_1n_end - section_1n_start);                       /* time_track */
      auto section_2_start = std::chrono::high_resolution_clock::now();        /* time_track */
      if (is_position_seen)
      {
        // let's try to optimize hash number of hash table access operations a little bit
        // wow, it worked!
        status_t next_position_result = state_to_result[next_position];
        if (next_position_result == WHITE)
        {
          if (current_position.current_player == 1)
          {
            current_result = WHITE;
          }
          // and no improvements for player == 2 i.e. black
        }
        else if (next_position_result == BLACK)
        {
          if (current_position.current_player == 2)
          {
            current_result = BLACK;
          }
          // and no improvements for player == 1 i.e. white
        }
        else if (next_position_result == DRAW)
        {
          if (((current_position.current_player == 1) && (current_result == BLACK))
              || ((current_position.current_player == 2) && (current_result == WHITE)))
          {
            current_result = DRAW;
          }
        }
        // UNKNOWN?
      }
      else
      {
        static size_t counter = 0;
        //***if (counter > 19967   ) {std::cin.get();} // asdf
        if (counter % 10000 == 0)
        {
          std::cout << to_string(option) << " | " << to_string(move_options) << "\n";
          std::cout << to_string(next_position) << "\n";
          std::cout << counter << "\n";
          if (counter > 50000000) {std::cin.get();}
          // if (counter % 100000 == 0)
          if (false)
          {
            std::cout << to_string(next_position);
            std::cin.get();
          }
          std::cout << section_0.count() << " " << section_1.count() << " "    /* time_track */
                  << section_1n.count() << " "                                 /* time_track */
                  << section_2.count() << " " << section_3.count() << "\n";    /* time_track */
        }
        ++counter;
        stack_of_positions.push(next_position);
        previous.insert(current_position);
        current_position = next_position;
        current_result = UNKNOWN;
        break;
      }
      auto section_2_end = std::chrono::high_resolution_clock::now();          /* time_track */
      section_2 += (section_2_end - section_2_start);                          /* time_track */
    }
    auto section_3_start = std::chrono::high_resolution_clock::now();          /* time_track */
    if (current_result != UNKNOWN)
    {
      state_to_result[current_position] = current_result;
      stack_of_positions.pop();
      current_position = stack_of_positions.top();

      /* size_t is_erased = ** W */ previous.erase(current_position);
      // assert(is_erased == 1); // ** W
    }
    auto section_3_end = std::chrono::high_resolution_clock::now();            /* time_track */
    section_3 += (section_3_end - section_3_start);                            /* time_track */
  }
}

// someshow can't be deleted yet
void lookup_step(
        std::unordered_map<CheckersPosition<std::vector<std::vector<square_t>>>,
                                            status_t>& state_to_result,
        CheckersSituation<std::vector<std::vector<square_t>>>& current_situation,
        std::stack<CheckersSituation<std::vector<std::vector<square_t>>>>& stack_of_situations)
{
  auto section_0 = std::chrono::duration<double, std::milli>();                    /* time_track */
  auto section_1 = std::chrono::duration<double, std::milli>();                    /* time_track */
  auto section_2 = std::chrono::duration<double, std::milli>();                    /* time_track */
  auto section_3 = std::chrono::duration<double, std::milli>();                    /* time_track */
  status_t victory = check_victory(current_situation);
  if (victory != UNKNOWN)
  {
    auto section_0_start = std::chrono::high_resolution_clock::now();            /* time_track */
    state_to_result[current_situation.position] = victory;
    stack_of_situations.pop();
    current_situation = stack_of_situations.top();
    auto section_0_end = std::chrono::high_resolution_clock::now();              /* time_track */
    section_0 += (section_0_end - section_0_start);                              /* time_track */
  }
  else
  {
    std::vector<CheckersMove> move_options = get_options(current_situation.position);
    status_t current_result = (current_situation.position.current_player == 1) ? BLACK : WHITE;
    for (auto option : move_options)
    {
      auto section_1_start = std::chrono::high_resolution_clock::now();          /* time_track */
      CheckersSituation next_situation = CheckersSituation(::make_move(option,
                                                                       current_situation));
      bool is_position_seen = state_to_result.contains(next_situation.position);
      auto section_1_end = std::chrono::high_resolution_clock::now();            /* time_track */
      section_1 += (section_1_end - section_1_start);                            /* time_track */

      auto section_2_start = std::chrono::high_resolution_clock::now();          /* time_track */
      if (is_position_seen)
      {
        if (state_to_result[next_situation.position] == WHITE)
        {
          if (current_situation.position.current_player == 1)
          {
            current_result = WHITE;
          }
          // and no improvements for player == 2 i.e. black
        }
        else if (state_to_result[next_situation.position] == BLACK)
        {
          if (current_situation.position.current_player == 2)
          {
            current_result = BLACK;
          }
          // and no improvements for player == 1 i.e. white
        }
        else if (state_to_result[next_situation.position] == DRAW)
        {
          if (((current_situation.position.current_player == 1) && (current_result == BLACK))
              || ((current_situation.position.current_player == 2) && (current_result == WHITE)))
          {
            current_result = DRAW;
          }
        }
        // UNKNOWN?
      }
      else
      {
        std::cout << to_string(option) << " | " << to_string(move_options) << "\n";
        std::cout << "\n" << sizeof(next_situation) << "\n";
        std::cout << to_string(next_situation) << "\n";
        static size_t counter = 0;
        std::cout << counter << "\n";
        std::cout << section_0.count() << " " << section_1.count() << " "    /* time_track */
                  << section_2.count() << " " << section_3.count() << "\n";  /* time_track */
        if (counter > 500000) {std::cin.get();}
        if ((counter % 1000000 == 0) && (counter > 0))
        {
          std::cout << to_string(next_situation, true);
          std::cin.get();
        }
        ++counter;
        stack_of_situations.push(next_situation);
        current_situation = next_situation;
        current_result = UNKNOWN;
        break;
      }
      auto section_2_end = std::chrono::high_resolution_clock::now();       /* time_track */
      section_2 += (section_2_end - section_2_start);                       /* time_track */
    }

    auto section_3_start = std::chrono::high_resolution_clock::now();       /* time_track */
    if (current_result != UNKNOWN)
    {
      state_to_result[current_situation.position] = current_result;
      stack_of_situations.pop();
      current_situation = stack_of_situations.top();
    }
    auto section_3_end = std::chrono::high_resolution_clock::now();         /* time_track */
    section_3 += (section_3_end - section_3_start);                         /* time_track */
  }
}

template <typename HashMapType>
void AutoPlayer<HashMapType>::fit()
{
  CheckersPosition current_position = CheckersGame<AutoBoardType>::get_initial_position();
  std::stack<CheckersPosition<AutoBoardType>> stack_of_positions(
                                            {CheckersGame<AutoBoardType>::get_initial_position()});
  std::unordered_set<CheckersPosition<AutoBoardType>> previous;
  while (state_to_result_[CheckersGame<AutoBoardType>::get_initial_position()] == UNKNOWN)
  {
    lookup_step(state_to_result_, current_position, stack_of_positions, previous);
  }

  // CheckersSituation current_situation = CheckersSituation<square_t>();
  // std::stack<CheckersSituation<square_t>> stack_of_situations({CheckersSituation<square_t>()});
  // // a bit incorrect of course
  // while (state_to_result_[CheckersGame::get_initial_position()] == UNKNOWN)
  // {
  //   lookup_step(state_to_result_, current_situation, stack_of_situations);
  // }

  // CheckersPosition current_position = CheckersGame::get_initial_position();
  // std::stack<CheckersPosition> stack_of_positions({CheckersGame::get_initial_position()});
  // while (state_to_result_[CheckersGame::get_initial_position()] == UNKNOWN)
  // {
  //   status_t victory = check_victory(current_position);
  //   if (victory != UNKNOWN)
  //   {
  //     state_to_result_[current_position] = victory;
  //     stack_of_positions.pop();
  //     current_position = stack_of_positions.top();
  //   }
  //   else
  //   {
  //     std::vector<std::vector<std::pair<size_t, size_t>>> move_options
  //                                                                  = get_options(current_position);
  //     // assume current player loses and try to find any possible improvements
  //     status_t current_result = (current_position.current_player == 1) ? BLACK : WHITE;
  //     for (auto option : move_options)
  //     {
  //       CheckersPosition next_position = ::make_move(option, current_position);
  //       if (state_to_result_.contains(next_position))
  //       {
  //         if (state_to_result_[next_position] == WHITE)
  //         {
  //           if (current_position.current_player == 1)
  //           {
  //             current_result = WHITE;
  //           }
  //           // and no improvements for player == 2 i.e. black
  //         }
  //         else if (state_to_result_[next_position] == BLACK)
  //         {
  //           if (current_position.current_player == 2)
  //           {
  //             current_result = BLACK;
  //           }
  //           // and no improvements for player == 1 i.e. white
  //         }
  //         else if (state_to_result_[next_position] == DRAW)
  //         {
  //           if (((current_position.current_player == 1) && (current_result == BLACK))
  //               || ((current_position.current_player == 2) && (current_result == WHITE)))
  //           {
  //             current_result = DRAW;
  //           }
  //         }
  //         // UNKNOWN?
  //       }
  //       else
  //       {
  //         std::cout << to_string(option) << " | " << to_string(move_options) << "\n";
  //         std::cout << to_string(next_position) << "\n";
  //         static size_t counter = 0;
  //         if (counter > 200) {std::cin.get();} // {char c; std::cin >> c;}
  //         ++counter;
  //         stack_of_positions.push(next_position);
  //         current_position = next_position;
  //         current_result = UNKNOWN;
  //         break;
  //       }
  //     }
  //     if (current_result != UNKNOWN)
  //     {
  //       state_to_result_[current_position] = current_result;
  //       stack_of_positions.pop();
  //       current_position = stack_of_positions.top();
  //     }
  //   }
  // }
  // if 
}

template <typename HashMapType>
CheckersMove AutoPlayer<HashMapType>::make_move(
                                           const CheckersGame<std::vector<std::vector<square_t>>>&)
{
  return {{1, 1}, {2, 2}};
}

template class AutoPlayer<std::unordered_map<CheckersPosition<AutoBoardType>, status_t>>;
template class AutoPlayer<HashMapAdapter<HashMapSingleton>>;
template class AutoPlayer<HashMapAdapter<HashMapChainSingleton>>;
template class AutoPlayer<HashMapAdapter<HashMapGradualSingleton>>;
