#include <iostream>
#include <vector>
#include <array>
#include <stack>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <limits>
#include "node.h"
#include "game.h"
#include "globals.h"

uint8_t get_value(const BoardMini& board, uint8_t i, uint8_t j)
{
  return board.get_value(i, j);
}

template <typename T>
uint8_t get_value(const std::vector<std::vector<T>>& board, uint8_t i, uint8_t j)
{
  return board[i][j];
}

template uint8_t get_value<size_t>(const std::vector<std::vector<size_t>>&, uint8_t, uint8_t);
template uint8_t get_value<uint8_t>(const std::vector<std::vector<uint8_t>>&, uint8_t, uint8_t);

void set_value(BoardMini& board, uint8_t i, uint8_t j, uint8_t value)
{
  board.set_value(i, j, value);
}

template <typename T>
void set_value(std::vector<std::vector<T>>& board, uint8_t i, uint8_t j, uint8_t value)
{
  board[i][j] = value;
}

template void set_value<size_t>(std::vector<std::vector<size_t>>&, uint8_t, uint8_t, uint8_t);
template void set_value<uint8_t>(std::vector<std::vector<uint8_t>>&, uint8_t, uint8_t, uint8_t);

size_t get_player_id(uint8_t piece_code)
{
  if ((piece_code == 1) || (piece_code == 3))
  {
    return WHITE;
  }
  else if ((piece_code == 2) || (piece_code == 4))
  {
    return BLACK;
  }
  else return std::numeric_limits<size_t>::max();
}

bool is_man(uint8_t piece_code)
{
  return ((piece_code == 1) || (piece_code == 2));
}

template <int N>
uint8_t size(const BoardMini& board)
{
  return 6;
}

template uint8_t size<0>(const BoardMini&);
template uint8_t size<1>(const BoardMini&);

// TODO make specialization
template <int N, typename T>
uint8_t size(const std::vector<std::vector<T>>& board)
{
  if (N == 0) return board.size();
  else if (N == 1) return board[0].size();
  else {assert(false); return 0;}
}

template uint8_t size<0, size_t>(const std::vector<std::vector<size_t>>&);
template uint8_t size<0, uint8_t>(const std::vector<std::vector<uint8_t>>&);
template uint8_t size<1, size_t>(const std::vector<std::vector<size_t>>&);
template uint8_t size<1, uint8_t>(const std::vector<std::vector<uint8_t>>&);

template <typename T>
std::array<char, 7> compress(const std::vector<std::vector<T>>& board)
{
  std::array<char, 7> result;
  for (size_t i = 0; i < board.size(); ++i)
  {
    for (size_t j = 0; j < board[0].size(); ++j)
    {
      if ((i + j) % 2 == 0)
      {
        if (board[i][j] == 1)
        {
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2));
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (board[i][j] == 2)
        {
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2));
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (board[i][j] == 3)
        {
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2));
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else if (board[i][j] == 4)
        {
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2));
          set_unit(result, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
        else
        {
          assert(board[i][j] == 0);
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2));
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 1);
          set_zero(result, 3 * (3 * i + (j - (i % 2)) / 2) + 2);
        }
      }
      else
      {
        assert(board[i][j] == 0);
      }
    }
  }
  return result;
}

template std::array<char, 7> compress<size_t>(const std::vector<std::vector<size_t>>&);
template std::array<char, 7> compress<uint8_t>(const std::vector<std::vector<uint8_t>>&);

std::array<char, 7> compress(const BoardMini& board)
{
  return board.compress();
}

template <typename BoardType>
bool is_on_board(const BoardType& board, size_t square_x, size_t square_y)
{
  return (square_x < size<0>(board)) && (square_y < size<1>(board));
}

template <typename BoardType>
bool is_empty(const BoardType& board, size_t square_x, size_t square_y)
{
  assert(is_on_board(board, square_x, square_y));
  return (get_value(board, square_x, square_y) == 0);
}

static bool is_opponents_piece(uint8_t first_piece_code, uint8_t second_piece_code)
{
  assert(first_piece_code != 0);
  return ((second_piece_code != 0) && (first_piece_code % 2 != second_piece_code % 2));
}

template <typename BoardType>
std::pair<size_t, size_t> get_capture_range(const BoardType& board,
                                            size_t square_x, size_t square_y,
                                            size_t direction)
{
  if (is_man(get_value(board, square_x, square_y)))
  {
    std::pair<size_t, size_t> result;
    if ((is_on_board(board, square_x + 2 * DIRECTIONS[direction].first,
                            square_y + 2 * DIRECTIONS[direction].second)
        && is_opponents_piece(get_value(board, square_x, square_y),
                              get_value(board, square_x + DIRECTIONS[direction].first,
                                               square_y + DIRECTIONS[direction].second))
        // is_empty field for jump
        && (get_value(board, square_x + 2 * DIRECTIONS[direction].first,
                             square_y + 2 * DIRECTIONS[direction].second) == 0)))
    {
      result = std::pair<size_t, size_t>(2, 3);
    }
    else
    {
      result = std::pair<size_t, size_t>(0, 0);
    }
    // std::cout << "=03a=: " << result.first << " " << result.second << "\n";
    return result;
  }
  else // is king
  {
    // assert(((board[square_x][square_y] - 1) / 2) == 1);             
    std::pair<size_t, size_t> result;                                 // begin and end
    size_t step = 1;
    
    while (is_on_board(board, square_x + DIRECTIONS[direction].first * step,
                              square_y + DIRECTIONS[direction].second * step)
           && is_empty(board, square_x + DIRECTIONS[direction].first * step,
                              square_y + DIRECTIONS[direction].second * step))
    {
      ++step;
    }
    if (is_on_board(board, square_x + DIRECTIONS[direction].first * step,
                           square_y + DIRECTIONS[direction].second * step)
        // is_opponents_piece
        && (get_value(board, square_x + DIRECTIONS[direction].first * step,
                             square_y + DIRECTIONS[direction].second * step) % 2
                 != get_value(board, square_x, square_y) % 2))
    {
      ++step;
      result.first = step;
      
      while (is_on_board(board, square_x + DIRECTIONS[direction].first * step,
                                square_y + DIRECTIONS[direction].second * step)
             && is_empty(board, square_x + DIRECTIONS[direction].first * step,
                                square_y + DIRECTIONS[direction].second * step))
      {
        ++step;
      }
      result.second = step;
      if (result.first != result.second)
      {
        // std::cout << "=04= capture_range: "<< result.first << " " << result.second << "\n";
        return result;
      }
      else
      {
        return std::pair<size_t, size_t>(0, 0);
      }
    }
    else
    {
      return std::pair<size_t, size_t>(0, 0);
    }
  }
}

template std::pair<size_t, size_t> get_capture_range<std::vector<std::vector<size_t>>>(
                                  const std::vector<std::vector<size_t>>&, size_t, size_t, size_t);

template std::pair<size_t, size_t> get_capture_range<std::vector<std::vector<uint8_t>>>(
                                 const std::vector<std::vector<uint8_t>>&, size_t, size_t, size_t);

PlayingPiece::PlayingPiece(uint8_t square_x, uint8_t square_y, uint8_t piece_code):
                                  square_x(square_x), square_y(square_y), piece_code(piece_code) {}

template <typename T>
bool Node<T>::is_leaf()
{
  return false;
}

template <typename BoardType>
void Node<BoardType>::update_board_push(BoardType& board)
{
  // std::cout << "Node::update_board_push(...)\n";
  std::pair<size_t, size_t> captured_square = get_captured_square();
  if (captured_square != NONE)
  {
    set_value(board, captured_square.first, captured_square.second, 0);
  }
  std::pair<size_t, size_t> new_square = get_square();
  if (new_square.first == 5)
  {
    set_value(board, new_square.first, new_square.second, 3);
  }
}

template <typename BoardType>
void StartingNode<BoardType>::update_board_push(BoardType& board)
{
}

template <typename BoardType>
void DirectionNode<BoardType>::update_board_push(BoardType& board)
{
  std::pair<size_t, size_t> captured_square = get_captured_square();
  if (captured_square != NONE)
  {
    set_value(board, captured_square.first, captured_square.second, 0);
  }
  std::pair<size_t, size_t> new_square = this->get_square();
  if ((new_square.first == 5) && this->is_white_)
  {
    set_value(board, new_square.first, new_square.second, 3);
  }
}

template <typename BoardType>
void Node<BoardType>::update_board_pop(BoardType& board)
{
  // TODO include promoting to board update
  // std::cout << "Node::update_board_pop(...)\n";
  std::pair<size_t, size_t> captured_square = get_captured_square();
  // std::cout << "=13= captured_square: " << captured_square.first << " "
  //                                       << captured_square.second << "\n";
  std::pair<size_t, size_t> worked_off_square = get_square();
  // std::cout << "=14b= worked_off_square: " << worked_off_square.first << " "
  //                                          << worked_off_square.second << "\n";

  // uint8_t piece_code; // ** W
  if (worked_off_square != NONE)
  {
    // piece_code = get_value(board, worked_off_square.first, worked_off_square.second); // ** W
  }
  if (captured_square != NONE)
  {
    set_value(board, captured_square.first, captured_square.second, 2);
  }
}

template <typename BoardType>
void DirectionNode<BoardType>::update_board_pop(BoardType& board)
{
  // std::cout << "DirectionNode::update_board_pop(...)\n";
  // std::cout << taken_piece_.square_x << " " << taken_piece_.square_y << " " 
  //           << taken_piece_.piece_code << "\n";
  std::pair<size_t, size_t> captured_square = get_captured_square();
  std::pair<size_t, size_t> worked_off_square = this->get_square();
  // uint8_t piece_code; // not used yet
  if (worked_off_square != NONE)
  {
    // piece_code = get_value(board, worked_off_square.first, worked_off_square.second); // same
  }
  if (captured_square != NONE)
  {
    set_value(board, captured_square.first, captured_square.second, taken_piece_.piece_code);
  }
}

template <typename BoardType>
void PlainNodeStart<BoardType>::update_board_pop(BoardType& board)
{
  // std::cout << "PlainNodeStart::update_board_pop(...)\n";
}

// template <typename BoardType>
// void PlainKingStart<BoardType>::update_board_pop(BoardType& board) {}

template <typename BoardType>
PieceNode<BoardType>::PieceNode(size_t square_x, size_t square_y, bool is_white):
                               Node<BoardType>(is_white), square_x(square_x), square_y(square_y) {}

template <typename T>
void PieceNode<T>::update_result_push(std::vector<std::pair<size_t, size_t>>& result)
{
  result.push_back(std::pair<size_t, size_t>(square_x, square_y));
}

template <typename T>
void PieceNode<T>::update_result_pop(std::vector<std::pair<size_t, size_t>>& result)
{
  result.pop_back();
}

template <typename T>
std::pair<size_t, size_t> PieceNode<T>::get_square() const
{
  return std::pair<size_t, size_t>(square_x, square_y);
}

template <typename BoardType>
Node<BoardType>* PlainNodeEnd<BoardType>::next(BoardType& board)
{
  return nullptr;
}

template <typename T>
std::pair<size_t, size_t> PlainNodeEnd<T>::get_captured_square() const
{
  return NONE;
}

template <typename T>
std::string PlainNodeEnd<T>::get_readable() const
{
  return std::string("PlainNodeEnd(") + std::to_string(this->square_x) + std::string(", ")
         + std::to_string(this->square_y) + std::string(")");
}

template <typename T>
bool PlainNodeEnd<T>::is_clear_needed() const
{
  return true;
}

template <typename T>
PlainNodeEnd<T>::PlainNodeEnd(size_t square_x, size_t square_y):
                                                          PieceNode<T>(square_x, square_y, true) {}

template <typename T>
DirectionNode<T>::DirectionNode(size_t square_x, size_t square_y, size_t direction,
              size_t move_length, size_t capture_length, PlayingPiece taken_piece,
              bool is_white):
                                         PieceNode<T>(square_x, square_y, is_white),
                                         square_x(square_x), square_y(square_y),
                                         forbidden_direction(direction),
                                         current_direction(0), current_jump(0),
                                         move_length(move_length), capture_length(capture_length),
                                         taken_piece_(taken_piece)
{}

template <typename BoardType>
Node<BoardType>* DirectionNode<BoardType>::next(BoardType& board)
{
  // std::cout << "=05= DirectionNode::next(...)\n";
  // std::cout << "=06= current_direction = " << current_direction << "\n";
  // std::cout << "=07= current_jump = " << current_jump << "\n";
  // std::cout << "=08= current_square = " << square_x << " " << square_y << "\n";
  if (current_direction == forbidden_direction)
  {
    ++current_direction;
    return next(board);
  }
  if (current_direction == 4)
  {
    return nullptr;
  }
  std::pair<size_t, size_t> capture_range = get_capture_range(board, square_x, square_y,
                                                              current_direction);
  if ((current_jump == 0) // didn't investigate this direction yet
      && (capture_range.first != capture_range.second)) // not empty
  {
    // ...
    current_jump = capture_range.first;
    return next(board);
  }
  if (current_jump < capture_range.second)
  {
    std::pair<size_t, size_t> taken_piece = {
                    square_x + DIRECTIONS[current_direction].first * (capture_range.first - 1),
                    square_y + DIRECTIONS[current_direction].second * (capture_range.first - 1)};
    // std::cout << "=09= " << taken_piece.first << " " << taken_piece.second << " "
    //                      << board[taken_piece.first][taken_piece.second] << "\n";
    DirectionNode* new_node = new DirectionNode(
                             square_x + DIRECTIONS[current_direction].first * current_jump,
                             square_y + DIRECTIONS[current_direction].second * current_jump,
                             (current_direction + 2) % 4, // can't go back next time
                             current_jump, current_jump - capture_range.first,
                             PlayingPiece(taken_piece.first, taken_piece.second,
                                          // board[taken_piece.first][taken_piece.second]),
                                          get_value(board, taken_piece.first, taken_piece.second)),
                             this->is_white_);
                                   
    // moving the piece
    set_value(board, square_x + DIRECTIONS[current_direction].first * current_jump,
                     square_y + DIRECTIONS[current_direction].second * current_jump,
                      get_value(board, square_x, square_y));
    set_value(board, square_x, square_y, 0);
    ++current_jump;
    return new_node;
  }
  else
  {
    ++current_direction;
    current_jump = 0;
    return next(board);
  }
  return nullptr;
}

template <typename BoardType>
void DirectionNode<BoardType>::update_result_pop(std::vector<std::pair<size_t, size_t>>& result)
{
  result.pop_back();
}

template <typename T>
std::pair<size_t, size_t> DirectionNode<T>::get_captured_square() const
{
  return {taken_piece_.square_x, taken_piece_.square_y};
}

template <typename T>
std::string DirectionNode<T>::get_readable() const
{
  return std::string(
      "DirectionNode " + std::to_string(square_x) + " " + std::to_string(square_y)
      + " " + std::to_string(forbidden_direction) + " " + std::to_string(move_length)
      + " " + std::to_string(capture_length) + " " + std::to_string(taken_piece_.square_x)
      + " " + std::to_string(taken_piece_.square_y) + " " + std::to_string(taken_piece_.piece_code)
      + " " + std::to_string(current_direction) + (this->is_white_ ? " white" : " black"));
}

template <typename T>
bool DirectionNode<T>::is_clear_needed() const
{
  return true;
}

template class DirectionNode<std::vector<std::vector<size_t>>>;
template class DirectionNode<std::vector<std::vector<uint8_t>>>;
template class DirectionNode<BoardMini>;

template <typename T>
StartingNode<T>::StartingNode(size_t square_x, size_t square_y, bool is_white): 
                                         PieceNode<T>(square_x, square_y, is_white),
                                         current_direction(0), current_jump(0)
{}

template <typename BoardType>
Node<BoardType>* StartingNode<BoardType>::next(BoardType& board)
{
  if (current_direction == 4)
  {
    return nullptr;
  }
  std::pair<size_t, size_t> capture_range = get_capture_range(board,
                                                              this->square_x, this->square_y,
                                                              current_direction);
  std::pair<size_t, size_t> taken_piece = {
                 this->square_x + DIRECTIONS[current_direction].first * (capture_range.first - 1),
                 this->square_y + DIRECTIONS[current_direction].second * (capture_range.first - 1)};
  if ((current_jump == 0) // didn't investigate this direction yet
      && (capture_range.first != capture_range.second)) // not empty
  {
    current_jump = capture_range.first;
    return next(board);
  }
  if (current_jump < capture_range.second)
  {
    DirectionNode<BoardType>* new_node = new DirectionNode<BoardType>(
                             this->square_x + DIRECTIONS[current_direction].first * current_jump,
                             this->square_y + DIRECTIONS[current_direction].second * current_jump,
                             (current_direction + 2) % 4, // can't go back next time
                             current_jump, current_jump - capture_range.first,
                             PlayingPiece(taken_piece.first, taken_piece.second,
                                          get_value(board, taken_piece.first, taken_piece.second)),
                             this->is_white_);
                                   
    // moving the piece
    set_value(board, this->square_x + DIRECTIONS[current_direction].first * current_jump,
                     this->square_y + DIRECTIONS[current_direction].second * current_jump,
              get_value(board, this->square_x, this->square_y));
    set_value(board, this->square_x, this->square_y, 0);
    ++current_jump;
    return new_node;
  }
  else
  {
    ++current_direction;
    current_jump = 0;
    return next(board);
  }
  return nullptr;
}

template <typename T>
std::pair<size_t, size_t> StartingNode<T>::get_captured_square() const // temporary solution
{
  // assert(false);
  // return std::pair<size_t, size_t>(0, 0); // that's None value; it is not going to be used
  return NONE;
}

template <typename T>
std::string StartingNode<T>::get_readable() const
{
  return std::string("StartingNode " + std::to_string(this->square_x)
                     + " " + std::to_string(this->square_y));
}

template <typename T>
bool StartingNode<T>::is_clear_needed() const
{
  return false;
}

// checks if a piece can perform capture move on a particular board
template <typename BoardType>
bool can_capture(size_t square_x, size_t square_y, const BoardType& board, bool is_white)
{
  if ((get_value(board, square_x, square_y) == 0) // can't capture with no piece or opponent's piece
      || ((get_player_id(get_value(board, square_x, square_y)) == WHITE) && !is_white)
      || ((get_player_id(get_value(board, square_x, square_y)) == BLACK) && is_white))
  {
    return false;
  }

  bool result = false;
  size_t capture_options_number = 0;
  for (size_t direction = 0; direction < 4; ++direction)
  {
    std::pair<size_t, size_t> capture_range = get_capture_range(board, square_x, square_y,
                                                                direction);
    capture_options_number += (capture_range.second - capture_range.first);
  }
  if (capture_options_number > 0)
  {
    result = true;
  }
  return result;
}

template <typename BoardType>
bool is_capture(const BoardType& board, bool is_white)
{
  bool result = false;
  for (size_t x = 0; x < size<0>(board); ++x)
  {
    for (size_t y = 0; y < size<1>(board); ++y)
    {
      result = can_capture(x, y, board, is_white);
      if (result == true)
      {
        // std::cout << "capture: " << x << " " << y << "\n"; // ** D
        return result;
      }
    }
  }
  return result;
}

template bool is_capture(const BoardMini&, bool);
template bool is_capture(const std::vector<std::vector<size_t>>&, bool);
template bool is_capture(const std::vector<std::vector<uint8_t>>&, bool);

template <typename T>
Node<T>::Node(bool is_white): is_white_(is_white) {}

template <typename T>
InitialNode<T>::InitialNode(bool is_white): Node<T>(is_white), square_x(0), square_y(0),
                                                       did_plain_left(false), move_type(UNKNOWN) {}

// provides next element in square iteration: (0, 0) -> (0, 1) -> ... -> (0, 5) -> (1, 0) -> ...
void increment_square(size_t& square_x, size_t& square_y, size_t width)
{
  ++square_y;
  if (square_y == width)
  {
    ++square_x;
    square_y = 0;
  }
}

// should be nice to add board squares iterator
template <typename BoardType>
Node<BoardType>* InitialNode<BoardType>::next(BoardType& board)
{
  if (this->square_x == size<0>(board))
  {
    return nullptr;
  }

  // should be run only once to figure out if there is a capture option
  if (this->move_type == UNKNOWN)
  {
    if (is_capture(board, this->is_white_))
    {
      // std::cout << "=05f= CAPTURE\n"; // ** D
      print_board(board);
      std::cout << (this->is_white_ ? "is white\n" : "is black\n");
      move_type = CAPTURE;
    }
    else
    {
      // std::cout << "=05f= PLAIN\n";
      move_type = PLAIN;
    }
  }

  if ((get_player_id(get_value(board, this->square_x, this->square_y)) == WHITE)
      && this->is_white_)
  {
    // std::cout << "=05c= WHITE\n"; // ** D
    if (this->move_type == PLAIN)
    {
      Node<BoardType>* result;
      if (is_man(get_value(board, square_x, square_y)))
      {
        result = new PlainNodeStart<BoardType>(square_x, square_y, true);
      }
      else
      {
        result = new PlainKingStart<BoardType>(square_x, square_y, true);
      }

      increment_square(square_x, square_y, size<1>(board));
      return result;
    }
    else // move_type == CAPTURE
    {
      // std::cout << "=05d= CAPTURE\n"; // ** D
      if (can_capture(square_x, square_y, board, this->is_white_))
      {
        StartingNode<BoardType>* new_node
                                = new StartingNode<BoardType>(square_x, square_y, this->is_white_);
        increment_square(square_x, square_y, size<1>(board));
        return new_node;
      }
      else
      {
        increment_square(square_x, square_y, size<1>(board));
        return next(board);
      }
    }
  }

  else if ((get_player_id(get_value(board, square_x, square_y)) == BLACK) && !this->is_white_)
  {
    // TODO process it
    if (move_type == PLAIN)
    {
      Node<BoardType>* result;
      if (is_man(get_value(board, square_x, square_y)))
      {
        result = new PlainNodeStart<BoardType>(square_x, square_y, false);
      }
      else
      {
        result = new PlainKingStart<BoardType>(square_x, square_y, false);
      }
      increment_square(square_x, square_y, size<1>(board));
      return result;
    }
    else // move_type == CAPTURE
    {
      if (can_capture(square_x, square_y, board, this->is_white_))
      {
        StartingNode<BoardType>* new_node
                                = new StartingNode<BoardType>(square_x, square_y, this->is_white_);
        increment_square(square_x, square_y, size<1>(board));
        return new_node;
      }
      else
      {
        increment_square(square_x, square_y, size<1>(board));
        return next(board);
      }
    }

    increment_square(square_x, square_y, size<1>(board));
    return next(board);
  }
  else // piece is not WHITE or BLACK (probably still looking for - do not remember already)
  {
    increment_square(square_x, square_y, size<1>(board));
    return next(board);
  }
}

template <typename T>
void InitialNode<T>::update_result_push(std::vector<std::pair<size_t, size_t>>& result)
{
  result.push_back(std::pair<size_t, size_t>(square_x, square_y));
}

template <typename T>
void InitialNode<T>::update_result_pop(std::vector<std::pair<size_t, size_t>>& result)
{
  result.pop_back();
}

// template <typename BoardType>
// void InitialNode<BoardType>::update_board_pop(BoardType& board) {}

template <typename T>
std::pair<size_t, size_t> InitialNode<T>::get_captured_square() const // temporary solution
{
  return NONE;
}

template <typename T>
std::pair<size_t, size_t> InitialNode<T>::get_square() const // temporary solution
{
  return NONE;
}

template <typename T>
std::string InitialNode<T>::get_readable() const
{
  return std::string("InitialNode " + std::to_string(square_x) + " " + std::to_string(square_y)
                     + " " + std::to_string(move_type) + (this->is_white_ ? " white" : " black"));
}

template <typename T>
bool InitialNode<T>::is_clear_needed() const
{
  return true;
}

template class InitialNode<std::vector<std::vector<size_t>>>;
template class InitialNode<std::vector<std::vector<uint8_t>>>;
template class InitialNode<BoardMini>;

template <typename T>
PlainNodeStart<T>::PlainNodeStart(size_t square_x, size_t square_y, bool is_white):
                                  PieceNode<T>(square_x, square_y, is_white),
                                  current_state(WAIT_MINUS), threw_failed_leaf(false) {}

template <typename BoardType>
Node<BoardType>* PlainNodeStart<BoardType>::next(BoardType& board)
{
  // asserting that piece is a man
  int direction = (this->is_white_ ? 1 : -1);
  // iterate over two mober options
  if (current_state == WAIT_MINUS)
  {
    // can't move left, so go right
    if ((this->square_y == 0)
        || (get_value(board, this->square_x + direction, this->square_y - 1) != 0))
    {
      current_state = DEPLETED;
      if ((this->square_y + 1 < size<1>(board))
          && (get_value(board, this->square_x + direction, this->square_y + 1) == 0))
      {
        return new PlainNodeEnd<BoardType>(this->square_x + direction, this->square_y + 1);
      }
      else
      {
        // return nullptr;
        if (threw_failed_leaf)
        {
          return nullptr;
        }
        else
        {
          threw_failed_leaf = true;
          return new FailedLeafNode<BoardType>();
        }
      }
    }
    else // moving left, just as planned
    {
      current_state = WAIT_PLUS;
      return new PlainNodeEnd<BoardType>(this->square_x + direction, this->square_y - 1);
    }
  }
  if (current_state == WAIT_PLUS)
  {
    current_state = DEPLETED;
    if ((this->square_y + 1 < size<1>(board))
         && (get_value(board, this->square_x + direction, this->square_y + 1) == 0))
    {
      return new PlainNodeEnd<BoardType>(this->square_x + direction, this->square_y + 1);
    }
    else
    {
      return nullptr;
    }
  }
  else // DEPLETED
  {
    return nullptr;
  }
}

template <typename T>
std::pair<size_t, size_t> PlainNodeStart<T>::get_captured_square() const
{
  return NONE;
}

template <typename T>
std::pair<size_t, size_t> PlainNodeStart<T>::get_square() const
{
  return NONE;
}

template <typename T>
std::string PlainNodeStart<T>::get_readable() const
{
  return std::string("PlainNodeStart(") + std::to_string(this->square_x) + std::string(", ")
                                        + std::to_string(this->square_y) + std::string(")");
}

template <typename T>
bool PlainNodeStart<T>::is_clear_needed() const
{
  return true;
}

template <typename T>
PlainKingStart<T>::PlainKingStart(size_t square_x, size_t square_y, bool is_white):
                                PieceNode<T>(square_x, square_y, is_white), step_(1), direction_(0)
{}

template <typename BoardType>
Node<BoardType>* PlainKingStart<BoardType>::next(BoardType& board)
{
  if (direction_ == 4)
  {
    return nullptr;
  }
  while (direction_ < 4)
  {
    // std::pair<size_t, size_t> new_square = {
    //                                this->square_x + DIRECTIONS[direction_].first * step_,   // W
    //                                this->square_y + DIRECTIONS[direction_].second * step_}; // W
    if (is_on_board(board, this->square_x + DIRECTIONS[direction_].first * step_,
                           this->square_y + DIRECTIONS[direction_].second * step_)
        && is_empty(board, this->square_x + DIRECTIONS[direction_].first * step_,
                           this->square_y + DIRECTIONS[direction_].second * step_))
    {
      Node<BoardType>* result = new PlainNodeEnd<BoardType>(
                                           this->square_x + DIRECTIONS[direction_].first * step_,
                                           this->square_y + DIRECTIONS[direction_].second * step_);
      ++step_;
      return result;
    }
    else
    {
      ++direction_;
      step_ = 1;
      return next(board);
    }
  }
  return nullptr;
}

template <typename T>
std::pair<size_t, size_t> PlainKingStart<T>::get_captured_square() const
{
  return NONE;
}

template <typename T>
std::string PlainKingStart<T>::get_readable() const
{
  return std::string("PlainKingStart(") + std::to_string(this->square_x) + std::string(", ")
                                        + std::to_string(this->square_y) + std::string(")");
}

template <typename T>
bool PlainKingStart<T>::is_clear_needed() const
{
  return false; // used to be true, test 28 failed
}

template <typename BoardType>
std::vector<std::vector<std::pair<size_t, size_t>>> depth_width_search(
                                       Node<BoardType>* root, const BoardType& board)
{
  // std::cout << "=00x= depth_width_search(...)\n";             // ** D
  // std::cout << "=00y= " << root->get_readable() << "\n";      // ** D
  // print_board(board);                                         // ** D

  std::vector<std::vector<std::pair<size_t, size_t>>> result;
  std::vector<std::pair<size_t, size_t>> current_result;
  std::stack<Node<BoardType>*> stack_of_nodes({root});
  std::stack<std::pair<size_t, size_t>> taken_pieces;
  // size_t promoting_move = 0; // ** W
  bool is_first_request = true; // may be leaf
  // needed to make a mutable copy of board just to preserve const qualifiers
  // rolling back to initial values is guaranteed anyway
  BoardType board_copy(board);
  do
  {
    // std::cout << "=02= stack_of_nodes.size(): " << stack_of_nodes.size() << "\n";
    // std::cout << "=02a= result.size(): " << result.size() << "\n";
    // std::cout << "=02b= is_first_request: " << (is_first_request ? "true\n" : "false\n");
    std::cout << "=02c= nodes.top(): " << stack_of_nodes.top()->get_readable() << "\n"; // ** D
    print_board(board_copy); // ** D

    Node<BoardType>* next_node = stack_of_nodes.top()->next(board_copy);
    if (next_node == nullptr) // no next node, have to pull back
    {
      // std::cout << "=10=\n";
      if ((is_first_request) && !current_result.empty()) // leaf, recording result
      {
        // std::cout << "=11=\n";
        result.push_back(current_result);
        // stack_of_nodes.pop();
        is_first_request = false;
        // std::cout << "=12=\n";
      }
      else // not leaf, going down the tree
      {
        // stack_of_nodes.pop();
      }
      // std::pair<size_t, size_t> captured_square = stack_of_nodes.top()->get_captured_square(); W
      // std::cout << "=13a= captured_square: " << captured_square.first << " "
      //                                       << captured_square.second << "\n";

      std::pair<size_t, size_t> worked_off_square = stack_of_nodes.top()->get_square();
      // std::cout << "=13b= worked_off_square: " << worked_off_square.first << " " // ** D
      //                                          << worked_off_square.second << "\n";
      Node<BoardType>* previous_node = stack_of_nodes.top();
      stack_of_nodes.pop(); // going up the tree anyway
      // taken_pieces.pop();
      // std::cout << "=13c=\n";
      // next_node->update_result_pop(current_result);
      current_result.pop_back();
      // std::cout << "=15=\n"; // ** D
      previous_node->update_board_pop(board_copy);
      print_board(board_copy); // ** D

      // *** temporal section ***
      // TODO include promoting to board update
      std::cout << "=16=\n"; // ** D
      uint8_t piece_code;
      if (worked_off_square != NONE)
      {
        // std::cout << "=17=\n"; // ** D
        piece_code = get_value(board_copy, worked_off_square.first, worked_off_square.second);
        if (previous_node->is_clear_needed())
        {
          set_value(board_copy, worked_off_square.first, worked_off_square.second, 0);
        }
      }
      print_board(board_copy); // ** D

      // if (captured_square != NONE)
      // {
      //   board_copy[captured_square.first][captured_square.second] = 2;
      // }
      std::cout << "=15d\n"; // ** D
      if (!stack_of_nodes.empty())
      {
        std::pair<size_t, size_t> node_square = stack_of_nodes.top()->get_square();
        // std::cout << "=15e= node_square: " << node_square.first << " "
        //                                    << node_square.second << "\n";
        // if (node_square != NONE)
        if ((node_square != NONE) && (piece_code != 0))
        {
          // temporary solution
          set_value(board_copy, node_square.first, node_square.second, piece_code);
        }
      }
      // *** temporal section ***
    }
    else if (next_node->is_leaf()) // node is leaf, but not recording result, FailedLeafNode
    {
      // std::cout << "=18=\n";
      stack_of_nodes.pop();
      current_result.pop_back();
      is_first_request = false;
    }
    else // continue the search
    {
      // std::cout << "=30=\n";
      stack_of_nodes.push(next_node);
      is_first_request = true;
      next_node->update_result_push(current_result);

      next_node->update_board_push(board_copy);

      if (next_node->get_square().first == 5)
      {
        // promoting_move = stack_of_nodes.size(); // ** W
      }
      // std::cout << "=37=\n";
    }
    // std::cout << "=19=\n";
  } while (!stack_of_nodes.empty());
  // std::cout << "=20=\n";

  // std::cout << "=21=\n";
  // std::cout << result.size() << "\n";
  // for (size_t i = 0; i < result.size(); ++i)
  // {
  //   std::cout << result[i].size();
  //   for (size_t j = 0; j < result[i].size(); ++j)
  //   {
  //     std::cout << " (" << result[i][j].first << " " << result[i][j].second << ")";
  //   }
  //   std::cout << "\n";
  // }

  // std::cout << "=21a=\n";
  return result;
}

template std::vector<std::vector<std::pair<size_t, size_t>>> depth_width_search(
                 Node<std::vector<std::vector<size_t>>>*, const std::vector<std::vector<size_t>>&);

template std::vector<std::vector<std::pair<size_t, size_t>>> depth_width_search(
               Node<std::vector<std::vector<uint8_t>>>*, const std::vector<std::vector<uint8_t>>&);

template std::vector<std::vector<std::pair<size_t, size_t>>> depth_width_search(
                                                               Node<BoardMini>*, const BoardMini&);

template <typename T>
void print_board(const std::vector<std::vector<T>>& board)
{
  std::cout << "   * * * * * *\n";
  for (size_t i = 0; i < board.size(); ++i)
  {
    std::cout << " * ";
    for (size_t j = 0; j < board[board.size() - i - 1].size(); ++ j)
    {
      std::cout << unsigned(board[board.size() - i - 1][j]) << " ";
    }
    std:: cout << "*\n";
  }
  std::cout << "   * * * * * *\n";
}

template void print_board(const std::vector<std::vector<size_t>>&);
template void print_board(const std::vector<std::vector<uint8_t>>&);

template<typename T, size_t N>
void print_board(const std::array<std::array<T, N>, N>& board)
{
  std::cout << "   * * * * * *\n";
  for (size_t i = 0; i < board.size(); ++i)
  {
    std::cout << " * ";
    for (size_t j = 0; j < board[board.size() - i - 1].size(); ++ j)
    {
      std::cout << unsigned(board[board.size() - i - 1][j]) << " ";
    }
    std:: cout << "*\n";
  }
  std::cout << "   * * * * * *\n";
}

template void print_board<uint8_t, 6>(const std::array<std::array<uint8_t, 6>, 6>&);

void print_board(const BoardMini& board)
{
  std::cout << "   * * * * * *\n";
  for (size_t i = 0; i < 6; ++i)
  {
    std::cout << " * ";
    for (size_t j = 0; j < 6; ++ j)
    {
      std::cout << unsigned(board.get_value(6 - i - 1, j)) << " ";
    }
    std:: cout << "*\n";
  }
  std::cout << "   * * * * * *\n";
}

// a bit more human-readable version
template <typename T>
void show_board(const std::vector<std::vector<T>>& board)
{
  std::cout << "   ** ** ** ** ** **\n";
  for (size_t i = 0; i < board.size(); ++i)
  {
    std::cout << " * ";
    for (size_t j = 0; j < board[board.size() - i - 1].size(); ++ j)
    {
      std::cout << board[board.size() - i - 1][j] << board[board.size() - i - 1][j] << " ";
    }
    std:: cout << "*\n";
  }
  std::cout << "   ** ** ** ** ** **\n";
}

template void show_board(const std::vector<std::vector<size_t>>&);
template void show_board(const std::vector<std::vector<uint8_t>>&);

void print_result(const std::vector<std::vector<std::pair<size_t, size_t>>>& result)
{
  std::cout << "options:\n";
  std::cout << result.size() << "\n";
  for (size_t i = 0; i < result.size(); ++i)
  {
    std::cout << result[i].size() << "\n";
    for (size_t j = 0; j < result[i].size(); ++j)
    {
      std::cout << result[i][j].first << " " << result[i][j].second << "  ";
    }
    std::cout << "\n";
  }
}

template <typename T>
FailedLeafNode<T>::FailedLeafNode(): PieceNode<T>(0, 0, true) {}

template <typename BoardType>
Node<BoardType>* FailedLeafNode<BoardType>::next(BoardType& board)
{
  return nullptr;
}

template <typename T>
std::pair<size_t, size_t> FailedLeafNode<T>::get_captured_square() const
{
  return NONE;
}

template <typename T>
std::string FailedLeafNode<T>::get_readable() const
{
  return std::string("FailedLeafNode");
}

template <typename T>
bool FailedLeafNode<T>::is_leaf()
{
  return true;
}

template <typename T>
bool FailedLeafNode<T>::is_clear_needed() const
{
  return true;
}

// only for testing purposes
// int main()
// {
//   std::vector<std::vector<size_t>> board;
//   std::vector<std::vector<std::pair<size_t, size_t>>> result;
// 
//   // test 1
//   // board = std::vector<std::vector<size_t>>({{3, 0, 0, 0, 0, 0}, {0, 2, 0, 0, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 2, 0, 2, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}});
//   // print_board(board);
//   // StartingNode starting_root(0, 0);
//   // result = depth_width_search(&starting_root, board);
//   // print_result(result);
// 
//   // test 2
//   // board = std::vector<std::vector<size_t>>({{3, 0, 3, 0, 0, 0}, {0, 2, 0, 0, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 2, 0, 2, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}});
//   // print_board(board);
//   // InitialNode initial_root;
//   // result = depth_width_search(&initial_root, board);
//   // print_result(result);
// 
//   // test 4
//   board = std::vector<std::vector<size_t>>({{3, 0, 1, 0, 0, 0}, {0, 2, 0, 0, 0, 0},
//                                             {0, 0, 0, 0, 0, 0}, {0, 2, 0, 2, 0, 0},
//                                             {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}});
//   print_board(board);
//   InitialNode initial_root_4;
//   result = depth_width_search(&initial_root_4, board);
//   print_result(result);
// 
//   // // test 6p
//   // board = std::vector<std::vector<size_t>>({{1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0},
//   //                                           {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 2}});
//   // InitialNode initial_root_0;
//   // // initial_root_0.next(board)->get_readable();
//   // std::cout << initial_root_0.next(board) << "\n";
// 
//   // test 6
//   std::cout << "starting test 6b\n";
//   board = std::vector<std::vector<size_t>>({{1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0},
//                                             {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0},
//                                             {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 2}});
//   print_board(board);
//   InitialNode initial_root;
// 
//   result = depth_width_search(&initial_root, board);
//   std::cout << "=40=\n";
//   print_result(result);
//   assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{1, 1}}})));
//   return 0;
// }

