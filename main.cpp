#include <vector>
#include <stack>
#include <algorithm>
#include <memory>
#include <cstddef>
#include "node.h"
#include "game.h"
#include "hashing.h"
#include "player.h"
#include "globals.h"

bool is_permutation(const std::vector<std::vector<std::pair<size_t, size_t>>>& first,
                    const std::vector<std::vector<std::pair<size_t, size_t>>>& second)
{
  return std::is_permutation(first.cbegin(), first.cend(), second.cbegin());
}

template <typename BoardType>
void test_board()
{
  BoardType board({{}});

  // test 00
  std::cout << "TEST 00\n";
  board = BoardType(
            {{0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0},
             {0, 0, 3, 0, 0, 0},
             {0, 2, 0, 2, 0, 0},
             {0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0}});
  print_board(board);
  DirectionNode<BoardType> root(2, 2, 2, 2, 1, PlayingPiece(1, 1, 2), true);
  std::vector<CheckersMove> result = depth_width_search(&root, board);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>(
                                                                 {{{4, 4}}, {{5, 5}}, {{4, 0}}})));
  // std::cin.get();

  // test 03
  std::cout << "TEST 03\n";
  board = BoardType({{0, 0, 0, 0, 0, 0},
                     {0, 0, 0, 3, 0, 3},
                     {0, 0, 0, 0, 2, 0},
                     {0, 0, 0, 0, 0, 0},
                     {0, 0, 2, 0, 2, 0},
                     {0, 0, 0, 0, 0, 0}});
  InitialNode<BoardType> initial_root(true);
  result = depth_width_search(&initial_root, board);
  std::cout << result.size() << "\n" << result[0].size() << " " << result[1].size() << " "
                                     << result[2].size() << " " << result[3].size() << "\n";
  std::cout << result[0][3].first << " " << result[1][3].first << " "
            << result[2][2].second << " " << result[3][2].second << "\n";
  assert(is_permutation(result,
                        {{{1, 3}, {3, 5}, {5, 3}, {3, 1}}, {{1, 3}, {3, 5}, {5, 3}, {2, 0}},
                         {{1, 5}, {3, 3}, {5, 1}}, {{1, 5}, {3, 3}, {5, 5}}}));
  // std::cin.get();

  // test 05
  std::cout << "TEST 05\n";
  board = BoardType({{0, 0, 0, 0, 0, 0},
                     {0, 0, 0, 1, 0, 3},
                     {0, 0, 0, 0, 2, 0},
                     {0, 0, 0, 0, 0, 0},
                     {0, 0, 2, 0, 2, 0},
                     {0, 0, 0, 0, 0, 0}});
  InitialNode<BoardType> initial_root_05(true);
  result = depth_width_search(&initial_root, board);
  assert(is_permutation(result,
                        {{{1, 5}, {3, 3}, {5, 1}}, {{1, 5}, {3, 3}, {5, 5}},
                         {{1, 3}, {3, 5}, {5, 3}, {3, 1}}, {{1, 3}, {3, 5}, {5, 3}, {2, 0}}}));
  // std::cin.get();
}

// TODO: create template function test(), run it several times for each container template
// TODO: think of registry or something

template <typename SquareType>
void test()
{
  std::vector<std::vector<SquareType>> board;
  std::vector<CheckersMove> result;

  // test 07
  std::cout << "     test 07\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_07(true);
  result = depth_width_search(&initial_root_07, board);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{0, 0}, {1, 1}}})));
  // std::cin.get();

  // test 8
  std::cout << "     test 08\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_08(true);
  result = depth_width_search(&initial_root_08, board);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{0, 0}, {2, 2}}})));
  // print_result(result);

  // test 9
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_09(true);
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 1, 0},
                                                 {0, 0, 0, 2, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  Node<std::vector<std::vector<SquareType>>>* next_node = initial_root_09.next(board);
  std::cout << next_node->get_readable() << "\n";
  std::cout << next_node->get_square().second << "\n";
  assert(next_node->get_square().second == 4);

  // test 10
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 1, 0},
                                                 {0, 0, 0, 2, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_10(true);
  result = depth_width_search(&initial_root_10, board);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{0, 4}, {2, 2}}})));

  // test 11p
  assert(is_capture<std::vector<std::vector<SquareType>>>({{1, 0, 0, 0, 0, 0},
                                                           {0, 1, 0, 0, 0, 0},
                                                           {0, 0, 0, 0, 0, 0},
                                                           {0, 0, 0, 0, 0, 0},
                                                           {0, 0, 0, 0, 0, 0},
                                                           {0, 0, 0, 0, 0, 2}}, true) == false);

  // test 11t
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {1, 0, 0, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_11t(true);
  result = depth_width_search(&initial_root_11t, board);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{3, 1}, {4, 0}},
                                                                         {{3, 1}, {4, 2}}})));

  // test 11
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_11(true);
  result = depth_width_search(&initial_root_11, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{1, 1}, {2, 0}},
                                                                         {{1, 1}, {2, 2}}})));

  // test 12
  std::cout << "TEST 12\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 1, 0, 1, 0},
                                                 {0, 1, 0, 1, 0, 1},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {2, 0, 2, 0, 2, 0},
                                                 {0, 2, 0, 2, 0, 2}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_12(true);
  result = depth_width_search(&initial_root_12, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{1, 1}, {2, 0}},
                                                                         {{1, 1}, {2, 2}},
                                                                         {{1, 3}, {2, 2}},
                                                                         {{1, 3}, {2, 4}},
                                                                         {{1, 5}, {2, 4}}})));

  // test 13
  CheckersGame<std::vector<std::vector<square_t>>> game_13;
  result = game_13.get_options();
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{1, 1}, {2, 0}},
                                                                         {{1, 1}, {2, 2}},
                                                                         {{1, 3}, {2, 2}},
                                                                         {{1, 3}, {2, 4}},
                                                                         {{1, 5}, {2, 4}}})));

  // test 14
  std::cout << "TEST 14\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {2, 0, 0, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_14(false);
  result = depth_width_search(&initial_root_14, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{4, 0}, {3, 1}},
                                                                         {{5, 1}, {4, 2}}})));

  // test 15
  std::cout << "TEST 15\n";
  CheckersGame<std::vector<std::vector<square_t>>> game_15;
  game_15.make_move({{1, 1}, {2, 0}});
  result = game_15.get_options();
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{4, 0}, {3, 1}},
                                                                         {{4, 2}, {3, 1}},
                                                                         {{4, 2}, {3, 3}},
                                                                         {{4, 4}, {3, 3}},
                                                                         {{4, 4}, {3, 5}}})));

  // test 16ppp
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {3, 0, 0, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  print_board(board);
  std::pair<size_t, size_t> capture_range = get_capture_range(board, 2, 0, 3);
  std::cout << capture_range.first << " " << capture_range.second << "\n";
  assert((capture_range.first == 0) && (capture_range.second == 0));

  // test 16pp
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {3, 0, 0, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_16pp(true);
  result = depth_width_search(&initial_root_16pp, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{2, 0}, {4, 2}},
                                                                         {{2, 0}, {5, 3}}})));

  // test 16p
  board = std::vector<std::vector<SquareType>> ({{0, 0, 1, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {3, 0, 1, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 3}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_16p(true);
  result = depth_width_search(&initial_root_16p, board);
  print_result(result);

  // test 16
  std::cout << "TEST 16\n";
  CheckersGame<std::vector<std::vector<SquareType>>> game_16;
  game_16.make_move({{1, 1}, {2, 2}});
  game_16.make_move({{4, 4}, {3, 5}});
  game_16.make_move({{1, 5}, {2, 4}});
  game_16.make_move({{5, 3}, {4, 4}});
  game_16.make_move({{0, 4}, {1, 5}});
  game_16.make_move({{4, 2}, {3, 1}});
  game_16.make_move({{0, 0}, {1, 1}});
  game_16.make_move({{5, 1}, {4, 2}});
  game_16.make_move({{2, 4}, {3, 3}});
  game_16.make_move({{4, 2}, {2, 4}});
  game_16.make_move({{1, 5}, {3, 3}});
  game_16.make_move({{3, 5}, {2, 4}});
  game_16.make_move({{1, 3}, {3, 5}, {5, 3}, {2, 0}});
  game_16.make_move({{5, 5}, {4, 4}});
  game_16.make_move({{3, 3}, {5, 5}});
  game_16.make_move({{4, 0}, {3, 1}});
  result = game_16.get_options();
  print_result(result);

  // test 17
  std::cout << "TEST 17\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 1, 0},
                                                 {0, 0, 0, 1, 0, 0},
                                                 {1, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 2, 0, 2, 0},
                                                 {0, 3, 0, 0, 0, 0}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_17(false);
  result = depth_width_search(&initial_root_17, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{4, 2}, {3, 1}},
                                                                         {{4, 2}, {3, 3}},
                                                                         {{4, 4}, {3, 3}},
                                                                         {{4, 4}, {3, 5}}})));


  // test 18
  std::cout << "TEST 18\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 1, 0, 0, 0},
                                                 {0, 0, 0, 1, 0, 1},
                                                 {1, 0, 1, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 2},
                                                 {2, 0, 1, 0, 2, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_18(false);
  result = depth_width_search(&initial_root_18, board);
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{3, 1}, {5, 3}}})));

  // test 19
  std::cout << "TEST 19\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 4, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 0},
                                                 {1, 0, 0, 0, 1, 0},
                                                 {0, 0, 0, 0, 0, 1},
                                                 {1, 0, 2, 0, 0, 0},
                                                 {0, 2, 0, 2, 0, 0}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_19(false);
  result = depth_width_search(&initial_root_19, board);
  print_result(result);

  // test 20
  std::cout << "TEST 20\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 1, 0, 0, 0, 1},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {1, 0, 2, 0, 1, 0},
                                                 {0, 2, 0, 2, 0, 0}});
  print_board(board);
  InitialNode<std::vector<std::vector<SquareType>>> initial_root_20(false);
  result = depth_width_search(&initial_root_20, board);
  print_result(result);

  // test 21
  std::cout << "TEST 21\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {3, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  assert(get_options(CheckersPosition(board, 2)).empty());

  // test 22
  std::cout << "TEST 22\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 1},
                                                 {0, 0, 1, 0, 4, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  print_board(board);
  capture_range = get_capture_range(board, 4, 4, 3);
  std::cout << capture_range.first << " " << capture_range.second << "\n";
  assert((capture_range.first == 0) && (capture_range.second == 0));

  // test 23
  std::cout << "TEST 23\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
                                                 {0, 4, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 1, 0, 1},
                                                 {0, 0, 1, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2}});
  result = get_options(CheckersPosition(board, 2));
  print_result(result);

  // test 24, now testing lookup process for different types
  CheckersSituation current_situation = CheckersSituation<std::vector<std::vector<square_t>>>();
  std::stack<CheckersSituation<std::vector<std::vector<square_t>>>> stack_of_situations(
                                        {CheckersSituation<std::vector<std::vector<square_t>>>()});
  std::unordered_map<CheckersPosition<std::vector<std::vector<square_t>>>,
                     status_t> state_to_result;
  state_to_result[CheckersGame<std::vector<std::vector<square_t>>>::get_initial_position()]
                                                                                         = UNKNOWN;
  for (size_t i = 0; i < 40; ++i)
  {
    lookup_step(state_to_result, current_situation, stack_of_situations);
    std::cout << to_string(current_situation);
  }
  // std::cin.get();

  // test 25 - problems with different square types:
  std::cout << "TEST 25\n";
  std::vector<std::vector<size_t>> board_32;
  auto board_uint8 = std::vector<std::vector<uint8_t>> ({{0, 0, 0, 0, 0, 0},
  // board = std::vector<std::vector<SquareType>> ({{0, 0, 0, 0, 0, 0},
  // board_32 = std::vector<std::vector<size_t>> ({{0, 0, 0, 0, 0, 0},
                                               {0, 0, 0, 1, 0, 1},
                                               {0, 0, 0, 0, 0, 0},
                                               {0, 0, 0, 1, 0, 1},
                                               {0, 0, 1, 0, 0, 0},
                                               {0, 0, 0, 2, 0, 2}});
  // result = get_options<size_t>(CheckersPosition(board_32, 2));
  // result = get_options<uint8_t>(CheckersPosition<uint8_t>(board, 2));
  result = get_options<std::vector<std::vector<uint8_t>>>(
                              CheckersPosition<std::vector<std::vector<uint8_t>>>(board_uint8, 2));
  print_result(result);
  assert(result == (std::vector<std::vector<std::pair<size_t, size_t>>>({{{5, 3}, {3, 1}}})));
  // std::cin.get();

  // test 26
  std::cout << "TEST 26\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 1},
                                                 {0, 0, 1, 0, 1, 0},
                                                 {0, 2, 0, 1, 0, 2},
                                                 {2, 0, 0, 0, 0, 0},
                                                 {0, 2, 0, 0, 0, 0}});
  result = get_options(CheckersPosition(board, 2));
  print_result(result);
  // std::cin.get();

  // test 27
  std::cout << "TEST 27\n";
  board = std::vector<std::vector<SquareType>> ({{1, 0, 1, 0, 0, 0},
                                                 {0, 0, 0, 1, 0, 1},
                                                 {0, 0, 1, 0, 3, 0},
                                                 {0, 2, 0, 0, 0, 2},
                                                 {2, 0, 0, 0, 2, 0},
                                                 {0, 0, 0, 0, 0, 0}});
  result = get_options(CheckersPosition(board, 1));
  print_result(result);
  // std::cin.get();

  // test 28
  std::cout << "TEST 28\n";
  board = std::vector<std::vector<SquareType>> ({{0, 0, 1, 0, 1, 0},
                                                 {0, 0, 0, 1, 0, 0},
                                                 {0, 0, 0, 0, 0, 0},
                                                 {0, 0, 0, 0, 0, 2},
                                                 {2, 0, 0, 0, 4, 0},
                                                 {0, 0, 0, 2, 0, 2}});
  result = get_options(CheckersPosition(board, 2));
  print_result(result);
  // std::cin.get();

  // board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 1, 0},
  //                                                {0, 0, 0, 1, 0, 1},
  //                                                {0, 0, 1, 0, 0, 0},
  //                                                {0, 0, 0, 2, 0, 2},
  //                                                {1, 0, 0, 0, 0, 0},
  //                                                {0, 2, 0, 2, 0, 0}});
  // apply_move({{2, 2}, {4, 4}}, board);
  // print_board(board);
  // std::cin.get();

  // std::cout << "TEST 26\n";
  // board = std::vector<std::vector<SquareType>> ({{1, 0, 0, 0, 1, 0},
  //                                                {0, 0, 0, 1, 0, 1},
  //                                                {0, 0, 0, 0, 0, 0},
  //                                                {0, 0, 0, 0, 0, 2},
  //                                                {1, 0, 0, 0, 1, 0},
  //                                                {0, 2, 0, 2, 0, 0}});
  // result = get_options(CheckersPosition(board, 2));
  // print_result(result);
  // std::cin.get();

  // // test 15 // don't know why it bugged on runtime
  // //   * * * * * *
  // // * 0 2 0 0 0 2 *
  // // * 0 0 0 0 0 0 *
  // // * 0 0 0 0 0 0 *
  // // * 0 0 0 0 0 0 *
  // // * 0 0 0 1 0 1 *
  // // * 4 0 0 0 0 0 *
  // //   * * * * * *
  // board = std::vector<std::vector<size_t>> ({{4, 0, 0, 0, 0, 0},
  //                                            {0, 0, 0, 1, 0, 1},
  //                                            {0, 0, 0, 0, 0, 0},
  //                                            {0, 0, 0, 0, 0, 0},
  //                                            {0, 0, 0, 0, 0, 0},
  //                                            {0, 2, 0, 0, 0, 2}});
  // print_board(board);
  // InitialNode initial_root_15(true);
  // result = depth_width_search(&initial_root_15, board);
  // print_result(result);

/*
   * * * * * *
 * 0 2 0 2 0 2 *
 * 2 0 2 0 1 0 *
 * 0 0 0 0 0 0 *
 * 0 0 0 0 0 0 *
 * 0 1 0 0 0 1 *
 * 1 0 1 0 1 0 *
   * * * * * *
*/
}

int main(int argc, char* argv[])
{
  // BoardMini board = BoardMini({{0, 0, 0, 0, 0, 0},
  //                              {0, 0, 0, 0, 0, 0},
  //                              {0, 0, 0, 0, 0, 0},
  //                              {0, 0, 0, 0, 0, 0},
  //                              {0, 0, 0, 0, 0, 0},
  //                              {0, 0, 0, 0, 0, 0}});
  // HashMapSingleton::instance();
  // HashMapSingleton::instance()->operator[](CheckersPosition(board, 2));
  // HashMapSingleton::instance()->operator[](CheckersPosition(board, 2)) = WHITE;
  // std::cout << (HashMapSingleton::instance()->contains(CheckersPosition(board, 2)) ?
  //               "true\n" : "false\n");
  // std::cout << unsigned(HashMapSingleton::instance()->operator[](CheckersPosition(board, 2))) << "\n";
  // HashMapExtSingleton();
  // std::cin.get();

  test_board<BoardMini>();
  test_board<std::vector<std::vector<size_t>>>();
  test_board<std::vector<std::vector<uint8_t>>>();

  test<size_t>();
  test<uint8_t>();
  test<square_t>();

  std::cout << "random seed: " << argv[1] << "\n";
  char* temp;
  size_t seed = strtol(argv[1], &temp, 10);

  CheckersGame<std::vector<std::vector<square_t>>> game;

  // // typedef std::unordered_map<CheckersPosition<AutoBoardType>, status_t> HashMapType;
  // // typedef HashMapAdapter<HashMapSingleton> HashMapType;
  // // typedef HashMapAdapter<HashMapChainSingleton> HashMapType;
  // typedef HashMapAdapter<HashMapGradualSingleton> HashMapType;
  // std::unique_ptr<AutoPlayer<HashMapType>> candidate_player(new AutoPlayer<HashMapType>(seed));
  // candidate_player->fit();

  GameRecording game_recording(1);
  CheckersMove move;

  std::unique_ptr<HumanPlayer> first_player(new HumanPlayer());
  // std::unique_ptr<RandomPlayer> second_player(new RandomPlayer(seed));
  std::unique_ptr<AIPlayer> second_player(new AIPlayer(seed));

  std::vector<std::unique_ptr<Player>> list_of_players;
  list_of_players.push_back(std::move(first_player));
  list_of_players.push_back(std::move(second_player));
  std::vector<GameRecording> games_archive;

  size_t counter = 0;
  while (true)
  {
    move = list_of_players[counter]->make_move(game);
    game.make_move(move);
    game_recording.update_moves(move);
    counter = (counter + 1) % 2;

    status_t game_result = game.check_victory();
    if (game_result != UNKNOWN)
    {
      game_recording.update_result(game_result);
      std::cout << game_recording.size() << "\n";
      games_archive.push_back(game_recording);
      // std::unique_ptr<AIPlayer> ai_player(new AIPlayer());
      // ai_player->fit(games_archive);
      // list_of_players[1] = std::move(ai_player);
      list_of_players[1]->fit(games_archive);
      std::cout << "preparing game reinitialization\n";

      // reinitializing game
      game = CheckersGame<std::vector<std::vector<square_t>>>();
      game_recording = GameRecording(1);
      std::cout << "game reinitialized\n";
      counter = 0;
    }
  }
  std::cout << "Game finished\n";
  return 0;
}

