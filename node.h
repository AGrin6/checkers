#include <array>
#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <limits>
#include "globals.h"

#ifndef NODE_H_
#define NODE_H_

template <typename T>
class Node;

class BoardMini;

const std::pair<size_t, size_t> NONE = {std::numeric_limits<size_t>::max(),
                                        std::numeric_limits<size_t>::max()};
const std::vector<std::pair<int, int>> DIRECTIONS = {{1, 1}, {1, -1}, {-1, -1}, {-1, 1}};

uint8_t get_value(const BoardMini&, uint8_t, uint8_t);

template <typename T>
uint8_t get_value(const std::vector<std::vector<T>>&, uint8_t, uint8_t);

void set_value(BoardMini& board, uint8_t, uint8_t, uint8_t);

template <typename T>
void set_value(std::vector<std::vector<T>>& board, uint8_t, uint8_t, uint8_t);

template <typename T>
void print_board(const std::vector<std::vector<T>>&);

template <typename T, size_t N>
void print_board(const std::array<std::array<T, N>, N>&);

void print_board(const BoardMini&);

template <typename T>
void show_board(const std::vector<std::vector<T>>&);

bool is_man(uint8_t);

template <int N>
uint8_t size(const BoardMini&);

template <int N, typename T>
uint8_t size(const std::vector<std::vector<T>>&);

template <typename T>
std::array<char, 7> compress(const std::vector<std::vector<T>>&);
std::array<char, 7> compress(const BoardMini&);

size_t get_player_id(uint8_t);

template <typename BoardType>
bool is_capture(const BoardType&, bool);

template <typename BoardType>
std::vector<std::vector<std::pair<size_t, size_t>>> depth_width_search(
                                                               Node<BoardType>*, const BoardType&);

template <typename BoardType>
std::pair<size_t, size_t> get_capture_range(const BoardType&, size_t, size_t, size_t);

void print_result(const std::vector<std::vector<std::pair<size_t, size_t>>>&);

struct PlayingPiece // to track taken pieces
{
  uint8_t square_x;
  uint8_t square_y;
  uint8_t piece_code;
  PlayingPiece(uint8_t, uint8_t, uint8_t);
};

template <typename BoardType>
class Node
{
 protected:
  bool is_white_;
 public:
  Node(bool);
  virtual bool is_leaf();
  virtual Node* next(BoardType&) = 0;
  virtual void update_result_push(std::vector<std::pair<size_t, size_t>>&) = 0;
  virtual void update_result_pop(std::vector<std::pair<size_t, size_t>>&) = 0;
  virtual std::pair<size_t, size_t> get_captured_square() const = 0;
  virtual std::pair<size_t, size_t> get_square() const = 0;
  virtual void update_board_push(BoardType&);
  virtual void update_board_pop(BoardType&);
  virtual std::string get_readable() const = 0;
  virtual bool is_clear_needed() const = 0;
};

template <typename BoardType>
class InitialNode: public Node<BoardType>
{
 private:
  mutable size_t square_x;
  mutable size_t square_y;
  mutable bool did_plain_left; // flag conserning if minus one option plain move has been processed
  enum move_t {UNKNOWN, PLAIN, CAPTURE};
  move_t move_type;
 public:
  InitialNode(bool);
  Node<BoardType>* next(BoardType&);
  void update_result_push(std::vector<std::pair<size_t, size_t>>&);
  void update_result_pop(std::vector<std::pair<size_t, size_t>>&);
  // void update_board_pop(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  std::pair<size_t, size_t> get_square() const;
  std::string get_readable() const;
  bool is_clear_needed() const;
};

// node with regard to particular piece
template <typename T>
class PieceNode: public Node<T>
{
 protected:
  size_t square_x;
  size_t square_y;
 public:
  PieceNode(size_t, size_t, bool);
  void update_result_push(std::vector<std::pair<size_t, size_t>>&);
  void update_result_pop(std::vector<std::pair<size_t, size_t>>&);
  std::pair<size_t, size_t> get_square() const;
};
  
// piece is fixed, but didn't move yet
// so there are no any forbidden directions and no data on captured pieces
template <typename BoardType>
class StartingNode: public PieceNode<BoardType>
{
 private:
  mutable size_t current_direction;
  mutable size_t current_jump;
 public:
  StartingNode(size_t, size_t, bool);
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const; // temporary
  void update_board_push(BoardType&);
  std::string get_readable() const;
  bool is_clear_needed() const;
};

template <typename BoardType>
class PlainNodeStart: public PieceNode<BoardType>
{
 private:
  enum state_t {WAIT_MINUS, WAIT_PLUS, DEPLETED};
  state_t current_state;
  bool threw_failed_leaf;
 public:
  PlainNodeStart(size_t, size_t, bool);
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  std::pair<size_t, size_t> get_square() const;
  void update_board_pop(BoardType& board);
  std::string get_readable() const;
  bool is_clear_needed() const;
};

template <typename BoardType>
class PlainNodeEnd: public PieceNode<BoardType>
{
 private:
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  std::string get_readable() const;
  bool is_clear_needed() const;
 public:
  PlainNodeEnd(size_t, size_t);
};

template <typename BoardType>
class PlainKingStart: public PieceNode<BoardType>
{
 private:
  size_t step_;
  size_t direction_;
 public:
  PlainKingStart(size_t, size_t, bool);
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  // void update_board_pop(BoardType&);
  std::string get_readable() const;
  bool is_clear_needed() const;
};

template <typename BoardType>
class DirectionNode: public PieceNode<BoardType>
{
 private:
  size_t square_x;
  size_t square_y;
  size_t forbidden_direction; // inverted piece came-in
  mutable size_t current_direction;
  mutable size_t current_jump;
  size_t move_length;         // length of previous move, needed to restore previous position
  size_t capture_length;      // length of previous capture, needed to restore previous position
  // std::pair<size_t, size_t> taken_piece;
  PlayingPiece taken_piece_;
  // capture candidate squares, only captures are possible when we already have fixed direction
  // returns empty range of possible steps (with starting equal to ending) if there is no options
 public:
  // DirectionNode(size_t, size_t, size_t, size_t, size_t, std::pair<size_t, size_t>, bool);
  DirectionNode(size_t, size_t, size_t, size_t, size_t, PlayingPiece, bool);
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  void update_board_push(BoardType&);
  void update_board_pop(BoardType&);
  void update_result_pop(std::vector<std::pair<size_t, size_t>>&);
  std::string get_readable() const;
  bool is_clear_needed() const;
};

// indicates that sequence of nodes leads to incorrect outcome
template <typename BoardType>
class FailedLeafNode: public PieceNode<BoardType>
{
 private:
  Node<BoardType>* next(BoardType&);
  std::pair<size_t, size_t> get_captured_square() const;
  std::string get_readable() const;
  bool is_clear_needed() const;
  bool is_leaf();
 public:
  FailedLeafNode();
};

#endif // NODE_H_
