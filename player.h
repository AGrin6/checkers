// #ifndef PLAYER_H_
// #define PLAYER_H_

#include </usr/local/opt/python@3.11/Frameworks/Python.framework/Versions/3.11/include/python3.11/Python.h>
#include <vector>
#include <cstdint>
#include <memory>
#include "game.h"
#include "node.h"

class HumanPlayer: public Player
{
 public:
  HumanPlayer();
  std::vector<std::pair<std::size_t, std::size_t>> get_human_move();
  std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                          const CheckersGame<std::vector<std::vector<square_t>>>&);
};

// assume 10 average moves or 20 half-moves (counting both), just to make a first guess
class AIPlayer: public Player
{
 private:
  std::unique_ptr<PyObject> model_;
  // PyObject* model_;
 public:
  AIPlayer();
  void fit(const std::vector<GameRecording>&);
  std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                          const CheckersGame<std::vector<std::vector<square_t>>>&);
};

// #endif // PLAYER_H_
