#include </usr/local/opt/python@3.11/Frameworks/Python.framework/Versions/3.11/include/python3.11/Python.h>
#include <vector>

// probably return type should be changed to unique_ptr, I'm not 100% sure
PyObject* to_list(const std::vector<float>& vector_1d);
PyObject* to_list(const std::vector<std::vector<float>>&);
