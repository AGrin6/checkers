#ifndef HASHING_H_
#define HASHING_H_

#include <array>
#include <tuple>
#include <cstdint>
#include <fstream>
#include <bitset>
#include "game.h"

typedef std::array<char, 7> key_t;
typedef std::tuple<key_t, status_t, chain_index_t> chaining_t; // before gradual

inline status_t& new_table_value(const CheckersPosition<BoardMini>&, std::vector<chaining_t>&);

class HashMapBaseSingleton
{
 private:
  static bool is_created_;
  class MultipleInstantiationException {};
 protected:
  constexpr static chain_index_t NONE = std::numeric_limits<chain_index_t>::max();
 public:
  HashMapBaseSingleton();
  static HashMapBaseSingleton* instance();
  virtual status_t& operator[](const CheckersPosition<BoardMini>&) = 0;
  virtual bool contains(const CheckersPosition<BoardMini>&) const = 0;
};

// constexpr size_t TABLE_SIZE = 2147483648;
// constexpr size_t TABLE_SIZE =  536870912;

class HashMapSingleton: public HashMapBaseSingleton
{
  // 2^44 17592186044416
  // 2^40 1099511627776
  // 2^36 68719476736
  // constexpr static size_t TABLE_SIZE = 4294967296; // 2^32
  // constexpr static size_t TABLE_SIZE = 3221225472;
  // constexpr static size_t TABLE_SIZE = 1073741824;
  // constexpr static size_t TABLE_SIZE =   67108864;
 private:
  static size_t hash(const CheckersPosition<BoardMini>&);
  std::array<std::array<char, 7>, TABLE_SIZE> hash_table_; // don't forget -O3 g++ optimization key
  std::array<status_t, TABLE_SIZE> hash_values_;
  std::array<chain_index_t, TABLE_SIZE> chaining_id_;
  std::vector<chaining_t> chaining_values_;
 protected:
  HashMapSingleton();
 public:
  static HashMapBaseSingleton* instance();
  status_t& operator[](const CheckersPosition<BoardMini>&);
  bool contains(const CheckersPosition<BoardMini>&) const;
};

// changed data organzation a little bit to make it more uniform
class HashMapChainSingleton: public HashMapBaseSingleton
{
 private:
  static size_t hash(const CheckersPosition<BoardMini>&);
  std::array<chain_index_t, TABLE_SIZE> hash_table_; // points to start of chain with hash
  std::vector<std::tuple<key_t, status_t, chain_index_t>> chaining_values_;
 protected: 
  HashMapChainSingleton();
 public:
  static HashMapBaseSingleton* instance();
  status_t& operator[](const CheckersPosition<BoardMini>&);
  bool contains(const CheckersPosition<BoardMini>&) const;
};

// with external data
class HashMapExtSingleton: public HashMapBaseSingleton
{
  class DoublyLinkedList // need doubly linked list to support reinsert() operation
  {
    // struct SinglyLinkedNode
    // {
    //   SinglyLinkedNode* next;
    //   chain_index_t value;
    // };
   private:
    uint32_t next_[16 * 256 * 256];
    chain_index_t data_[16 * 256 * 256];
    uint32_t back_;
    uint32_t front_;
    bool is_full_;
    // SinglyLinkedNode* front_;
    // SinglyLinkedNode* back_;
    // uint32_t size_;
   public:
    // SinglyLinkedList(): back_(nullptr), front_(bullptr), size_(0) {}
    DoublyLinkedList(): back_(0), front_(0), is_full_(false) {}
    void push/*_front*/(chain_index_t);
    void reinsert(uint32_t); // reinserts element from the middle of the queue to the front
  };
  class LinkedList // singly linked list of size 2^16 to save space a little bit
  {
   private:
    uint16_t next_[256 * 256];
    chain_index_t data_[256 * 256];
    uint16_t back_;
    uint16_t front_;
    bool is_full_;
   public:
    LinkedList(): back_(0), front_(0), is_full_(false) {}
    void push(chain_index_t index)
    {
      if (is_full_)
      {
        // a bit more complicated here
        assert(false);
      }
      else
      {
        data_[back_] = index;
        next_[back_] = back_ - 1; // should work to set NONE == std::max(uint16_t) == 0 - 1
        ++back_;
        if (back_ == std::numeric_limits<uint16_t>::max())
        {
          is_full_ = true;
        }
      }
    }
  };
  class DoublyLinkedList16 // size 2 ** 16 - 1, uint16_t indexing
  {
  };
 private:
  static size_t hash(const CheckersPosition<BoardMini>&);
  std::array<chain_index_t, TABLE_SIZE> hash_table_; // points to start of chain with hash
  std::vector<std::tuple<key_t, status_t, chain_index_t>> chaining_values_;
 protected: 
  HashMapExtSingleton();
 public:
  static HashMapBaseSingleton* instance();
  status_t& operator[](const CheckersPosition<BoardMini>&);
  bool contains(const CheckersPosition<BoardMini>&) const;
};

// will try some gradual changes from Chain to Ext
class HashMapGradualSingleton: public HashMapBaseSingleton
{
 private:
  // == 2^4 * 2^10 * 2^10 == 2^24 == (2^8)^3
  constexpr static size_t CHAIN_MAX_SIZE_ = /*16*/ 8 * 1024 * 1024;
  // points to start of chain with hash, duplicate from "game.h",
  // lowered two times due dumping stage freeze
  constexpr static size_t TABLE_SIZE = 134217728;
  // constexpr static size_t TABLE_SIZE = 67108864;
  typedef std::tuple<key_t, status_t, chain_index_t> chaining_t;
  static size_t hash_(const CheckersPosition<BoardMini>&);
  static inline status_t& new_table_value_(const CheckersPosition<BoardMini>&,
                                           std::vector<chaining_t>&,
                                           std::vector<uint16_t>&);
  std::array<chain_index_t, TABLE_SIZE> hash_table_;
  std::vector<chaining_t> chaining_values_;
  // contains pointers to cache queue
  // mutable std::array<uint16_t, std::numeric_limits<uint16_t>::max()> queue_indices_;
  mutable std::vector<uint16_t> queue_indices_;
  class DoublyLinkedList
  {
   private:
    uint16_t previous_[std::numeric_limits<uint16_t>::max()];
    uint16_t next_[std::numeric_limits<uint16_t>::max()];
    chain_index_t chain_[std::numeric_limits<uint16_t>::max()];
    uint16_t front_;
    uint16_t back_;
    bool is_full_;
   public:
    DoublyLinkedList(): front_(0), back_(0), is_full_(false) {}
    // returns index of created and deleted values
    // asserting that value is not already in the queue
    std::pair<uint16_t, chain_index_t> push(chain_index_t value)
    {
      if (is_full_)
      {
        // a bit more complicated here
        chain_index_t deleted_value = chain_[front_];
        chain_[front_] = value;
        previous_[back_] = front_;
        next_[front_] = back_;
        back_ = front_;
        front_ = previous_[front_];
        previous_[back_] = UINT16_NONE;
        next_[front_] = UINT16_NONE;
        return std::pair<uint16_t, uint16_t>(back_, deleted_value);
      }
      else
      {
        std::cout << "back_: " << back_ << "\n";
        chain_[back_] = value;
        next_[back_] = back_ - 1;
        previous_[back_ - 1] = back_;
        ++back_;
        if (back_ == std::numeric_limits<uint16_t>::max())
        {
          is_full_ = true;
        }
        return std::pair<uint16_t, uint16_t>(back_ - 1, chain_[back_ - 1]);
        // return std::pair<uint16_t, chain_index_t>(back_ - 1, CHAIN_NONE);
      }
    }
    void reinsert(uint16_t queue_index)
    {
      if (queue_index == front_)
      {
        front_ = previous_[front_]; // == previous_[queue_index]
        next_[front_] = UINT16_NONE;
        previous_[back_] = queue_index;
        next_[queue_index] = back_;
        previous_[queue_index] = UINT16_NONE;
        back_ = queue_index;
      }
      else if (queue_index == back_)
      {
        // do nothing, it is already in place
      }
      else
      {
        previous_[next_[queue_index]] = previous_[queue_index];
        next_[previous_[queue_index]] = next_[queue_index];
        previous_[back_] = queue_index;
        next_[queue_index] = back_;
        previous_[queue_index] = UINT16_NONE;
        back_ = queue_index;
      }
    }
  };
  DoublyLinkedList usage_queue_; // mutable? (conserns caching and not interface performance)
  // umbrella for push and reinsert containing presence in queue checking logic
  // once linked_list is not an argument, probably should be called "actualize_queue" or something
  uint16_t actualize_(/*DoublyLinkedList& linked_list, */chain_index_t index) // const
  {
    std::cout << "HashMapGradualSingleton::actualize(" << index << ")\n";
    std::cout << "chaining_values_.size(): " << chaining_values_.size() << "\n";
    // std::cin.get();
    if (chaining_values_.size() > CHAIN_MAX_SIZE_)
    {
      std::cout << "time to dump\n";
      std::ofstream output_stream;
      output_stream.open("./dump.bin");
      for (size_t i = 0; i < hash_table_.size(); ++i)
      {
        std::cout << "i: " << i << '\n';
        // if (hash_table_[i] == 0) {std::cin.get();}
        chain_index_t chain_start = hash_table_[i];
        chain_index_t chain_end = hash_table_[i];
        while (chain_end != CHAIN_NONE) // add condition
        {
          std::cout << "chain_start: " << chain_start << '\n';
          std::cout << "chain_end: " << chain_end << '\n';
          if (queue_indices_[chain_end] == UINT16_NONE)
          {
            std::cout << "==1==\n";
            key_t output_key = std::get<key_t>(chaining_values_[chain_end]);
            print_board(to_board(output_key));
            for (size_t j = 0; j < output_key.size(); ++j)
            {
              output_stream << output_key[j];
              std::cout << std::bitset<8>(output_key[j]) << " ";
            }
            // std::cin.get();
            std::cout << "\n==2==\n";
            output_stream << std::get<status_t>(chaining_values_[chain_end]);
            // output_stream << std::get<chain_index_t>(chaining_values_[chain_end]);
            chain_end = std::get<chain_index_t>(chaining_values_[chain_end]);
          }
          else
          {
            std::cout << "==3==\n";
            std::get<key_t>(chaining_values_[chain_start]) =
                                                      std::get<key_t>(chaining_values_[chain_end]);
            std::get<status_t>(chaining_values_[chain_start]) =
                                                   std::get<status_t>(chaining_values_[chain_end]);
            chain_start = std::get<chain_index_t>(chaining_values_[chain_start]);
            chain_end = std::get<chain_index_t>(chaining_values_[chain_end]);
            std::cout << "==4==\n";
          }
        }
      }
      output_stream.close();
      exit(1);
    }
    // uint16_t queue_index = std::get<uint16_t>(chaining_values_[index]);
    uint16_t queue_index = queue_indices_[index];
    std::cout << "queue_index: " << queue_index << "\n";
    if (queue_index == UINT16_NONE)
    {
      std::pair<uint16_t, chain_index_t> result = /*linked_list*/usage_queue_.push(index);
      std::cout << "index: " << index << "\n";
      std::cout << "queue_indices_.size(): " << queue_indices_.size() << "\n";
      queue_indices_[index] = result.first;
      queue_indices_[result.second] = UINT16_NONE;
      return result.first;
    }
    else
    {
      usage_queue_.reinsert(queue_index);
    }
    return UINT16_NONE;
  }
 protected: 
  HashMapGradualSingleton();
 public:
  static HashMapBaseSingleton* instance();
  status_t& operator[](const CheckersPosition<BoardMini>&);
  bool contains(const CheckersPosition<BoardMini>&) const;
};

// wraps HashMapSingleton to use it in position lookup process instead of standard hash table
template <typename HashMapType>
class HashMapAdapter
{
 public:
  status_t& operator[](const CheckersPosition<BoardMini>&);
  bool contains(const CheckersPosition<BoardMini>&) const;
};

#endif // HASHING_H_
