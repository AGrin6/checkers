#include <array>
#include <vector>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <initializer_list>
#include <cstddef>
#include "globals.h"

#ifndef GAME_H_
#define GAME_H_

// tuning constants
constexpr size_t TABLE_SIZE =  134217728;
// constexpr size_t TABLE_SIZE =  268435456;

enum status_t: char // game result status
{
  ABSENT = 0,
  WHITE = 1,
  BLACK = 2,
  DRAW = 3,
  UNKNOWN = 4
};

void set_zero(std::array<char, 7>&, uint8_t);
void set_unit(std::array<char, 7>&, uint8_t);

std::array<std::array<uint8_t, 6>, 6> to_board(const std::array<char, 7>&);

// economic version of board for lowering storage costs and faster elements access
class BoardMini
{
 private:
  char data[7]; /// 000 empty 1xy full x man/king y black/white. 3 * 18 = 54 bits <= 7 bytes
 public:
  BoardMini(std::initializer_list<std::initializer_list<uint8_t>>); // assert half of array zeros
  uint8_t get_value(uint8_t, uint8_t) const;
  void set_value(uint8_t, uint8_t, uint8_t);
  bool operator==(const BoardMini& other) const;
  // copy constructor
  // maybe size
  std::array<char, 7> compress() const; // for effective key use in hash table
};

typedef std::vector<std::pair<std::size_t, std::size_t>> CheckersMove; // TODO size_t -> uint8_t

template <typename BoardType>
void apply_move(const CheckersMove&, BoardType&);

template <typename BoardType>
struct CheckersPositionStable
{
  BoardType field;
  uint8_t current_player;
  CheckersPositionStable(const BoardType&, uint8_t);
  bool operator==(const CheckersPositionStable& other) const;
  // probably should be moved to free (non-member) functions for better flexibility
  std::array<char, 7> compress() const; // for effective key use in hash table
};

template <typename BoardType>
status_t check_victory(const CheckersPosition<BoardType>&,
                       const std::unordered_set<CheckersPosition<BoardType>>&);

template <typename T>
std::vector<std::vector<std::pair<std::size_t, std::size_t>>> get_options(
                                                                 const CheckersPositionStable<T>&);

namespace std
{
  template <>
  // assumes constant vector size across all inputs
  struct hash<CheckersPositionStable<std::vector<std::vector<square_t>>>>
  {
    size_t operator()(
                    const CheckersPositionStable<std::vector<std::vector<square_t>>>& input) const;
  };

  template <>
  // assumes constant vector size across all inputs
  struct hash<CheckersPositionStable<std::vector<std::vector<size_t>>>>
  {
    size_t operator()(const CheckersPositionStable<std::vector<std::vector<size_t>>>& input) const;
  };

  template <>
  struct hash<CheckersPositionStable<BoardMini>>
  {
    size_t operator()(const CheckersPositionStable<BoardMini>& input) const;
  };
}

// includes context such as past positions to check repetition
// maybe number of moves without significant change etc
template <typename T>
struct CheckersSituation
{
  CheckersPositionStable<T> position;
  std::unordered_set<CheckersPositionStable<T>> previous;
  CheckersSituation();
};

// struct CheckersSituationVer1
// {
//   CheckersPosition<square_t> position;
//   std::vector<CheckersPosition<square_t>> previous;
//   CheckersSituationVer1();
// };

std::string to_string(const std::vector<std::pair<size_t, size_t>>&);
std::string to_string(const std::vector<std::vector<std::pair<size_t, size_t>>>&);

template <typename BoardType>
std::string to_string(const CheckersPosition<BoardType>&);

// template <typename T>
// std::string to_string(const T&);
template <typename T>
std::string to_string(const CheckersSituation<T>&);

template <typename T>
class CheckersGame
{
 private:
  CheckersPosition<T> position_;
 public:
  CheckersGame();
  void draw_board() const;
  static CheckersPosition<T> get_initial_position();
  std::vector<std::vector<std::pair<std::size_t, std::size_t>>> get_options() const;
  void make_move(const std::vector<std::pair<std::size_t, std::size_t>>&);
};

class GameRecording
{
 private:
  size_t ai_player_id_;
  std::vector<std::vector<std::pair<size_t, size_t>>> moves_sequence_;
  size_t result_; // 1 white wins, 2 black wins, 3 draw, maybe 0 unknown
 public:
  GameRecording(size_t);
  void update_moves(const std::vector<std::pair<size_t, size_t>>&);
  void update_result(size_t result);
};

class Player
{
 public:
  Player();
  virtual std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                      const CheckersGame<std::vector<std::vector<square_t>>>&) = 0;
};

class HumanPlayer: public Player
{
 public:
  HumanPlayer();
  std::vector<std::pair<std::size_t, std::size_t>> get_human_move();
  std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                          const CheckersGame<std::vector<std::vector<square_t>>>&);
};

class RandomPlayer: public Player
{
 public:
  RandomPlayer(size_t);
  std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                          const CheckersGame<std::vector<std::vector<square_t>>>&);
};

class AIPlayer: public Player
{
 public:
  AIPlayer(size_t);
  std::vector<std::pair<std::size_t, std::size_t>> make_move(const CheckersGame<square_t>&);
};

// closely assosiated to AutoPlayer class function
void lookup_step(
               std::unordered_map<CheckersPosition<std::vector<std::vector<square_t>>>, status_t>&,
               CheckersSituation<std::vector<std::vector<square_t>>>&,
               std::stack<CheckersSituation<std::vector<std::vector<square_t>>>>&);

// typedef std::vector<std::vector<square_t>> AutoBoardType;
typedef BoardMini AutoBoardType;

template <typename HashMapType>
class AutoPlayer: public Player
{
 private:
  // std::unordered_map<CheckersPosition<AutoBoardType>, status_t> state_to_result_;
  HashMapType state_to_result_;
 public:
  AutoPlayer(size_t);
  void fit();
  std::vector<std::pair<std::size_t, std::size_t>> make_move(
                                          const CheckersGame<std::vector<std::vector<square_t>>>&);
};

#endif // GAME_H_

