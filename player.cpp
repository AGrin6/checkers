#include </usr/local/opt/python@3.11/Frameworks/Python.framework/Versions/3.11/include/python3.11/Python.h>
#include <cassert>
#include <cstddef>
#include <sstream>
#include <iomanip>
#include "player.h"
#include "py_io_utils.h"

// overall more agressive exploration (early stages at least), still freezes
const float WEIGHT_DECAY = 0.93; // first assumption - to half in 10 rounds
// 5% freezes on reasonably long game, sees some draw options before
const float EXPLORATION_RATE = 0.20; // absent (zero) or small (up to 10%!) leads to repetitions
const float EXPLORATION_DECAY = 1 - 1.0 / 4096.0; // 256.0; // 15.0 / 16.0; // 4096 large 256 small

// strategy of choosing option based on probability distribution
class OptionStrategy
{
  virtual size_t choose_option(const std::vector<float>&, std::mt19937&) = 0;
};

class PercentageOptionStrategy: public OptionStrategy // decaying percentage random move
{
 private:
  float current_;
  float decay_;
 public:
  PercentageOptionStrategy(float initial, float decay): current_(initial), decay_(decay) {}
  size_t choose_option(const std::vector<float>& list_of_ratings,
                       std::mt19937& random_generator)
  {
    return 0;
  }
};

class SecondOptionStrategy // higher the higher rating is
{
  size_t choose_option(const std::vector<float>& list_of_ratings,
                       std::mt19937& random_generator)
  {
    return 1;
  }
};

// prints move without final line break
void print_move(const CheckersMove& move)
{
  for (size_t i = 0; i < move.size(); ++i)
  {
    std::cout << unsigned(move[i].first) << unsigned(move[i].second) << " ";
  }
}

void Player::fit(const std::vector<GameRecording>& games_archive) {}

HumanPlayer::HumanPlayer() {}

CheckersMove to_checkers_move(const std::string& input)
{
  std::istringstream stream(input);

  std::vector<std::string> move_codes;
  std::string temp;
  while (std::getline(stream, temp, ' '))
  {
    move_codes.push_back(temp);
  }

  CheckersMove result;
  for (size_t i = 0; i < move_codes.size(); ++i)
  {
    result.push_back({move_codes[i][0] - '0', move_codes[i][1] - '0'});
  }
  return result;
}

CheckersMove HumanPlayer::get_human_move()
{
  std::cout << "Move in form xy zt uv...: ";
  std::string input;
  std::getline(std::cin, input);
  return to_checkers_move(input);
}

RandomPlayer::RandomPlayer(size_t seed)
{
  random_generator.seed(static_cast<std::mt19937::result_type>(seed));
}

CheckersMove get_random_move(const CheckersGame<std::vector<std::vector<square_t>>>& game,
                             std::mt19937& random_generator)
{
  std::vector<CheckersMove> list_of_options = game.get_options();
  // int random_number = rand();
  // std::cout << "DEBUG: random_number: " << random_number << "\n";
  // size_t option_id = random_number % list_of_options.size();
  std::uniform_int_distribution<> distribution(0, list_of_options.size() - 1);
  size_t option_id = distribution(random_generator);
  return list_of_options[option_id];
}

CheckersMove RandomPlayer::make_move(const CheckersGame<std::vector<std::vector<square_t>>>& game)
{
  CheckersMove result = get_random_move(game, random_generator);
  std::cout << "Computer random move: ";
  for (size_t i = 0; i < result.size(); ++i)
  {
    // to print uint8_t appropriately
    std::cout << unsigned(result[i].first) << unsigned(result[i].second) << " ";
  }
  std::cout << "\n";
  return result;
}

bool contains(const CheckersMove& move, const std::vector<CheckersMove>& options)
{
  for (const CheckersMove& value : options)
  {
    if (value == move)
    {
      return true;
    }
  }
  return false;
}

CheckersMove HumanPlayer::make_move(const CheckersGame<std::vector<std::vector<square_t>>>& game)
{
  // game.draw_board();
  // CheckersMove result = get_human_move();
  // if (!contains(result, game.get_options()))
  // {
  //   std::cout << "wrong input, try again\n";
  //   result = get_human_move();
  // }
  // return result;
  
  // return game.graphical_input();

  CheckersMove result = game.graphical_input();
  while (!contains(result, game.get_options()))
  {
    result = game.graphical_input();
  }
  std::cout << "Human move:           ";
  for (size_t i = 0; i < result.size(); ++i)
  {
    // to print uint8_t appropriately
    std::cout << unsigned(result[i].first) << unsigned(result[i].second) << " ";
  }
  std::cout << "\n";
  return result;
}

std::vector<float> dummify_piece(uint8_t value)
{
  std::vector<float> result = {0, 0, 0, 0};
  if (value != 0)
  {
    result[value - 1] = 1.0;
  }
  return result;
}

std::vector<float> dummify_component(uint8_t square)
{
  std::vector<float> result = {0, 0, 0, 0, 0, 0};
  result[square] = 1.0;
  return result;
}

template <typename BoardType>
std::vector<float> to_sample(const CheckersPosition<BoardType>& position, const CheckersMove& move)
{
  std::vector<float> result;
  std::vector<float> new_values;

  for (auto const& square : std::vector<std::vector<uint8_t>>(
         {{0, 0}, {0, 2}, {0, 4}, {1, 1}, {1, 3}, {1, 5}, {2, 0}, {2, 2}, {2, 4},
          {3, 1}, {3, 3}, {3, 5}, {4, 0}, {4, 2}, {4, 4}, {5, 1}, {5, 3}, {5, 5}}))
  {
    new_values = dummify_piece(get_value(position.field, square[0], square[1]));
    result.reserve(result.size() + new_values.size());
    result.insert(result.end(), new_values.begin(), new_values.end());
  }

  for (auto const& square : move)
  {
    new_values = dummify_component(square.first);
    result.reserve(result.size() + new_values.size());
    result.insert(result.end(), new_values.begin(), new_values.end());

    new_values = dummify_component(square.second);
    result.reserve(result.size() + new_values.size());
    result.insert(result.end(), new_values.begin(), new_values.end());
  }

  if (move.size() == 2)
  {
    new_values = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    result.reserve(result.size() + new_values.size());
    result.insert(result.end(), new_values.begin(), new_values.end());
  }

  if (move.size() == 3)
  {
    new_values = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    result.reserve(result.size() + new_values.size());
    result.insert(result.end(), new_values.begin(), new_values.end());
  }

  return result;
}

AIPlayer::AIPlayer(size_t seed)
{
  // std::cout << "DEBUG: AIPlayer::AIPlayer(" << seed << ")\n";
  // srand(seed);
  random_generator.seed(static_cast<std::mt19937::result_type>(seed));

  // need to use some Python ML routines unfortunately
  Py_Initialize();

  char module_name[] = "train";
  PyObject* p_name = PyUnicode_DecodeFSDefault(module_name);
  PyObject* p_module = PyImport_Import(p_name);
  Py_DECREF(p_name);

  if (p_module != NULL)
  {
    PyObject* py_module_dict = PyModule_GetDict(p_module);
    PyObject* py_model_holder = PyDict_GetItemString(py_module_dict, "ModelHolder");
    Py_DECREF(py_module_dict);

    // checks if instance of the class is callable
    if (!PyCallable_Check(py_model_holder))
    {
      std::cout << "Cannot instantiate the Python class" << std::endl;
      PyErr_Print();
      exit(1);
    }
    py_model_holder_ = std::unique_ptr<PyObject>(py_model_holder);
    py_model_holder = nullptr;
  }
  else
  {
    PyErr_Print();
    fprintf(stderr, "Failed to load \"%s\"\n", "train");
    exit(1);
  }
  // py_module_.reset(p_module);
}

// AIPlayer::~AIPlayer()
// {
//   std::cout << "AIPlayer::~AIPlayer()\n";
//   Py_FinalizeEx();
// }

void AIPlayer::fit(const std::vector<GameRecording>& games_archive)
{
  std::cout << "AIPlayer::fit(...)\n";
  // few special cases first
  if (games_archive.size() == 0)
  {
    // assert(false);
    model_ = std::unique_ptr<PyObject>(nullptr);
  }
  else // if (games_archive.size() == 1)
  {
    std::cout << "forming training samples\n";
    std::vector<std::vector<float>> list_of_samples;
    std::vector<float> list_of_labels;
    for (size_t i = 0; i < games_archive.size(); ++i)
    {
      std::cout << "i: " << i << "\n";
      // std::cout << "DEBUG: list_of_samples.size(): " << list_of_samples.size() << "\n";
      // std::cout << "DEBUG: list_of_labels.size(): " << list_of_labels.size() << "\n";
      CheckersPosition<std::vector<std::vector<uint8_t>>> position
                       = CheckersGame<std::vector<std::vector<uint8_t>>>::get_initial_position();
      for (size_t j = 0; j < games_archive[i].size(); ++j)
      {
        if (j % 2 == 1)
        {
          list_of_samples.push_back(to_sample(position, games_archive[i].get_move(j)));
        }
        apply_move(games_archive[i].get_move(j), position.field);
      }
      size_t known_labels = list_of_labels.size();
      list_of_labels.resize(list_of_samples.size(), 0);
      float label = (games_archive[i].get_result() == DRAW ? 0.0F : -1.0F);
      for (size_t j = list_of_labels.size() - 1; // avoiding weird issue with assigning -1 value
                  ((j >= known_labels) && (j < list_of_labels.size())); --j)
      {
        list_of_labels[j] = label;
        label *= WEIGHT_DECAY;
      }
    }
    std::cout << "forming training samples done\n";

    std::cout << "list_of_labels: ";
    for (size_t i = 0; i < list_of_labels.size(); ++i)
    {
      std::cout << std::fixed << std::setprecision(5) << list_of_labels[i] << " ";
    }
    std::cout << "\n";

    // Creates an instance of the class
    PyObject* holder_raw = py_model_holder_.release();
    if (!model_)
    {
      model_ = std::unique_ptr<PyObject>(PyObject_CallObject(holder_raw, nullptr));
    }
    py_model_holder_.reset(holder_raw);

    PyObject* p_samples = to_list(list_of_samples);
    
    PyObject* p_labels = to_list(list_of_labels);
    // PyObject* p_labels = PyList_New(0);
    // for (const auto& value : list_of_labels)
    // {
    //   PyList_Append(p_labels, PyFloat_FromDouble(value));
    // }

    PyObject* p_args = PyTuple_New(2);
    PyTuple_SetItem(p_args, 0, p_samples);
    PyTuple_SetItem(p_args, 1, p_labels);

    PyObject* model_raw = model_.release();
    PyObject_CallMethodObjArgs(model_raw, PyUnicode_FromString("fit"),
                               p_samples, p_labels, NULL);
    model_.reset(model_raw);
    if (PyErr_Occurred())
    {
      PyErr_Print();
      std::cout << "some problem with fit() function\n";
    }
    std::cout << "fit successful\n";
  }
}

// float get_rating(PyObject* model, position, const CheckersMove& option)

PyObject* py_get_list(const std::vector<float>& cpp_list)
{
  PyObject* result = PyList_New(0);
  for (const auto& value : cpp_list)
  {
    PyList_Append(result, PyFloat_FromDouble(value));
  }
  return result;
}

float get_prediction(const std::unique_ptr<PyObject>& model, const std::vector<float>& sample)
{
  PyObject* py_sample = py_get_list(sample);
  PyObject* py_args = PyTuple_New(1);
  PyTuple_SetItem(py_args, 0, py_sample);
  // Py_DECREF(py_sample); ?
  
  if (model)
  {
    PyObject* py_rating = PyObject_CallMethodOneArg(
                                            model.get(), PyUnicode_FromString("predict"), py_args);
    return PyFloat_AsDouble(py_rating);
   }
   else
   {
     return 0.0;
   }
}

// uses both list of options and position instead of just game situation
// to make it consistent regarding different options order and other reuse cases
template <typename BoardType>
std::vector<float> get_ratings(const std::unique_ptr<PyObject>& model,
                               std::vector<CheckersMove>& list_of_options,
                               const CheckersPosition<BoardType>& position)
{
  std::vector<float> result;
  for (const auto& option : list_of_options)
  {
    std::vector<float> sample = to_sample(position, option);
    result.push_back(get_prediction(model, sample));
  }
  return result;
}

size_t choose_option(const std::vector<float>& list_of_ratings,
                     std::mt19937& random_generator)
{
  float maximal_rating = -2.0F; // fixes bug, sometimes ratings may be less than -1.0F (!)
  size_t result = 32768;
  std::vector<size_t> equal_options;
  for (size_t i = 0; i < list_of_ratings.size(); ++i)
  {
    if (list_of_ratings[i] > maximal_rating)
    {
      maximal_rating = list_of_ratings[i];
      result = i;
      equal_options.clear();
    }
    else if (list_of_ratings[i] == maximal_rating)
    {
      if (equal_options.empty())
      {
        equal_options.push_back(result);
      }
      equal_options.push_back(i);
    }
  }

  if (equal_options.empty())
  {
    return result;
  }
  else
  {
    // size_t option_id = rand() % equal_options.size();
    std::uniform_int_distribution<> distribution(0, equal_options.size());
    size_t option_id = distribution(random_generator);
    if (option_id == equal_options.size())
    {
      return result;
    }
    else
    {
      return equal_options[option_id];
    }
  }
}

size_t choose_exploration_option(const std::vector<float>& list_of_ratings,
                                 std::mt19937& random_generator)
{
  static float decay = 1.0;
  decay *= EXPLORATION_DECAY;
  std::uniform_real_distribution<float> real_distribution(0.0, 1.0);
  float value = real_distribution(random_generator);
  // std::cout << "(value, rate) = (" << value << ", " << EXPLORATION_RATE * decay << ")\n";
  if (value < EXPLORATION_RATE * decay)
  {
    std::uniform_int_distribution<> distribution(0, list_of_ratings.size() - 1);
    return distribution(random_generator);
  }
  else
  {
    return choose_option(list_of_ratings, random_generator);
  }
}

CheckersMove AIPlayer::make_move(const CheckersGame<std::vector<std::vector<square_t>>>& game)
{
  if (model_) // is not nullptr, some data fitted
  {
    std::vector<CheckersMove> list_of_options = game.get_options();
    std::vector<float> list_of_ratings = get_ratings(model_, list_of_options,
                                                     game.get_current_position());
    for (size_t i = 0; i < list_of_options.size(); ++i)
    {
      print_move(list_of_options[i]);
      std::cout << list_of_ratings[i] << "\n";
    }
    // size_t best_option = choose_option(list_of_ratings);
    // std::cout << "DEBUG: choose_exploration_option(..., ...)\n";
    size_t best_option = choose_exploration_option(list_of_ratings, random_generator);
    // std::cout << "DEBUG: best_option: " << best_option << "\n";

    std::cout << "Computer smart move:  ";
    print_move(list_of_options[best_option]);
    std::cout << "\n";
    return list_of_options[best_option];
  }
  else
  {
    CheckersMove result = get_random_move(game, random_generator);
    std::cout << "Computer random move: ";
    for (size_t i = 0; i < result.size(); ++i)
    {
      // to print uint8_t appropriately
      std::cout << unsigned(result[i].first) << unsigned(result[i].second) << " ";
    }
    std::cout << "\n";
    return result;
  }
}
