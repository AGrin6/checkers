#include "node.h"
#include "hashing.h"

bool HashMapBaseSingleton::is_created_ = false;

HashMapBaseSingleton::HashMapBaseSingleton()
{
  if (is_created_)
  {
    throw MultipleInstantiationException();
  }
  else
  {
    is_created_ = true;
  }
}

inline size_t hash(const CheckersPosition<BoardMini>& input)
{
  constexpr size_t MULTIPLIER_PRIME =  523; // 31
  size_t result = 89; // 7; // first prime
  result = result * MULTIPLIER_PRIME + input.current_player; //second prime
  result = result * MULTIPLIER_PRIME + size<0>(input.field);
  result = result * MULTIPLIER_PRIME + size<1>(input.field);
  for (size_t i = 0; i < size<0>(input.field); ++i)
  {
    for (size_t j = 0; j < size<1>(input.field); ++j)
    {
      result = result * MULTIPLIER_PRIME + get_value(input.field, i, j);
      // result ^= 1;
      // result ^= (j % 2 == 0 ? 0b00000001 : 0b001101010);
      // result ^= (result << 1);
    }
  }
  // result ^= (get_value(input.field, 3, 3) << 3);
  // result ^= (get_value(input.field, 0, 2));
  // result ^= (2 * get_value(input.field, 0, 0) + 3 * get_value(input.field, 0, 2)
  //            + 5 * get_value(input.field, 0, 4) + 7 * get_value(input.field, 1, 1)
  //            + 11 * get_value(input.field, 1, 3) + 13 * get_value(input.field, 1, 5));

  result ^= 1;

  //***std::cout << result << "\n";
  result = (result % 4294967311) % TABLE_SIZE; // large prime number
  //***std::cout << result << "\n=======\n";
  // result /= 4;
  // uint8_t square_value = get_value(input.field, 5, 5);
  // result += ((TABLE_SIZE / 4) * (square_value == 0 ? 0 : square_value - 1));
  return result;
}

HashMapChainSingleton::HashMapChainSingleton()
{
  assert(sizeof(size_t) == 8);
  for (size_t i = 0; i < TABLE_SIZE; ++i)
  {
    hash_table_[i] = NONE;
  }
}

HashMapBaseSingleton* HashMapChainSingleton::instance()
{
  static HashMapBaseSingleton* instance_ = new HashMapChainSingleton();
  return instance_;
}

size_t HashMapChainSingleton::hash(const CheckersPosition<BoardMini>& input)
{
  return ::hash(input);
}

status_t& HashMapChainSingleton::operator[](const CheckersPosition<BoardMini>& key)
{
  size_t index = hash(key);
  if (hash_table_[index] == NONE)
  {
    hash_table_[index] = chaining_values_.size();
    chaining_values_.push_back(chaining_t(key.compress(), UNKNOWN, NONE));
    std::cout << chaining_values_.size() << "\n";
    return std::get<status_t>(chaining_values_.back());
  }
  else
  {
    chain_index_t chaining_index = hash_table_[index];
    while (key.compress() != std::get<key_t>(chaining_values_[chaining_index]))
    {
      if (std::get<chain_index_t>(chaining_values_[chaining_index]) == NONE)
      {
        std::get<chain_index_t>(chaining_values_[chaining_index]) = chaining_values_.size();
        chaining_values_.push_back(chaining_t(key.compress(), UNKNOWN, NONE));
        std::cout << chaining_values_.size() << "\n";
        return std::get<status_t>(chaining_values_.back());
      }
      chaining_index = std::get<chain_index_t>(chaining_values_[chaining_index]);
    }
    return std::get<status_t>(chaining_values_[chaining_index]);
  }
}

bool HashMapChainSingleton::contains(const CheckersPosition<BoardMini>& key) const
{
  size_t index = hash(key);
  return (hash_table_[index] != NONE);
}

HashMapSingleton::HashMapSingleton()
{
  assert(sizeof(size_t) == 8);
  for (size_t i = 0; i < TABLE_SIZE; ++i)
  {
    hash_values_[i] = ABSENT;
    chaining_id_[i] = NONE;
    for (size_t j = 0; j < 7; ++j)
    {
      hash_table_[i][j] = 0;
    }
    if (i % (1024 * 1024 * 64) == 0) {std::cout << i << "\n";}
  }
}

HashMapBaseSingleton* HashMapSingleton::instance()
{
  static HashMapBaseSingleton* instance_ = new HashMapSingleton();
  return instance_;
}

size_t HashMapSingleton::hash(const CheckersPosition<BoardMini>& input)
{
  return ::hash(input);
}

status_t& HashMapSingleton::operator[](const CheckersPosition<BoardMini>& key)
{
  // std::cout << "HashMapSingleton::operator[]()\n";
  // std::cout << to_string(key) << "\n";
  // std::cout << chaining_values_.size() << "\n";

  size_t index = hash(key); // moved dividing dividing logic to hash(...)

  // std::cout << index << "\n";
  // std::cout << unsigned(hash_values_[index]) << "\n";
  if (hash_values_[index] == ABSENT)
  {
    hash_values_[index] = UNKNOWN;
    hash_table_[index] = key.compress();
    return hash_values_[index];
  }
  else
  {
    // std::cout << key.compress() << "\n" << hash_table_[index] << "\n";
    // std::cout << "=2=\n";
    if (key.compress() == hash_table_[index])
    {
      return hash_values_[index];
    }
    // std::cout << "=4=\n";
    chain_index_t chain_index = chaining_id_[index];
    // std::cout << unsigned(chain_index) << "\n";
    // std::cout << "=5=\n";
    while (((chain_index != NONE)
            && key.compress() != std::get<key_t>(chaining_values_[chain_index])))
    {
      // std::cout << "=6=" << unsigned(chain_index) << "\n";
      chain_index = std::get<chain_index_t>(chaining_values_[chain_index]);
    }
    if (chain_index == NONE)
    {
      chaining_id_[index] = chaining_values_.size(); // (1)
      chaining_values_.push_back(chaining_t(key.compress(), UNKNOWN, NONE));
      return std::get<status_t>(chaining_values_.back());
    }
    else
    {
      return std::get<status_t>(chaining_values_[chain_index]);
    }
    // (1) used to be here
  }
  return hash_values_[index];
}

bool HashMapSingleton::contains(const CheckersPosition<BoardMini>& key) const
{
  size_t index = hash(key);
  return (hash_values_[index] != 0);
}

template <typename HashMapType>
status_t& HashMapAdapter<HashMapType>::operator[](const CheckersPosition<BoardMini>& key)
{
  return HashMapType::instance()->operator[](key);
}

template <typename HashMapType>
bool HashMapAdapter<HashMapType>::contains(const CheckersPosition<BoardMini>& key) const
{
  return HashMapType::instance()->contains(key);
}

template class HashMapAdapter<HashMapSingleton>;
template class HashMapAdapter<HashMapChainSingleton>;
