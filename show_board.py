import sys
import numpy
import PySimpleGUI
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plotter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

def show_board(np_board):
  fig = matplotlib.figure.Figure(figsize=(5, 4), dpi=100)
  fig.add_subplot(111).imshow(np_board, vmin=0, vmax=256, origin='lower', cmap='Greys_r')
 
  # Define the window layout
  layout = [
      [PySimpleGUI.Text("Plot test")],
      [PySimpleGUI.Canvas(key="-CANVAS-")],
      [PySimpleGUI.Input('', key='-INPUT-')],
      [PySimpleGUI.Button("Ok")],
  ]
  
  # Create the form and show it without the plot
  window = PySimpleGUI.Window(
      "Matplotlib Single Graph",
      layout,
      location=(700, 000), # originally (0, 0) - AG
      finalize=True,
      element_justification="center",
      font="Helvetica 18",
  )
  
  # Add the plot to the window
  figure_canvas_agg = FigureCanvasTkAgg(fig, window["-CANVAS-"].TKCanvas)
  figure_canvas_agg.draw()
  figure_canvas_agg.get_tk_widget().pack(side="top", fill="both", expand=1)
  
  event, values = window.read()
  window.close()
  return values['-INPUT-']

def show_board_list(arg_list):
  np_board = 128 * numpy.ones((6, 6), dtype=numpy.uint8)
  arg_list = [str(int(value)) for value in arg_list]
  for i in range(0, len(arg_list)):
    if arg_list[i] == '1':
      np_board[i // 6][i % 6] = 196
    elif arg_list[i] == '2':
      np_board[i // 6][i % 6] = 64
    elif arg_list[i] == '3':
      np_board[i // 6][i % 6] = 255
    elif arg_list[i] == '4':
      np_board[i // 6][i % 6] = 0

  return show_board(np_board)

if __name__ == '__main__':
  print(f'show_board.py __main__; sys.argv: {sys.argv}')
  np_board = 128 * numpy.ones((6, 6), dtype=numpy.uint8)
  for i in range(1, len(sys.argv)):
    if sys.argv[i] == '1':
      np_board[(i - 1) // 6][(i - 1) % 6] = 196
    elif sys.argv[i] == '2':
      np_board[(i - 1) // 6][(i - 1) % 6] = 64
    elif sys.argv[i] == '3':
      np_board[(i - 1) // 6][(i - 1) % 6] = 255
    elif sys.argv[i] == '4':
      np_board[(i - 1) // 6][(i - 1) % 6] = 0

  show_board(np_board)
